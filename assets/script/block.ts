// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
import GameMgr from "./GameMgr"

@ccclass
export default class block extends cc.Component {

    private anim;
    private animstate;

    private already_destroy:boolean=false;

    //道具
    @property(cc.Prefab)
    add_num:cc.Prefab=null;
    @property(cc.Prefab)
    add_power:cc.Prefab=null;
    @property(cc.Prefab)
    add_speed:cc.Prefab=null;

    mgr: GameMgr = null;

    itemGen=null;

    have_item:number=-1;
    which_item:number=-1;
    
    onLoad()
    {
        this.mgr = cc.find("GameMgr").getComponent(GameMgr);
        this.itemGen=cc.find('Canvas/itemGen');
        this.anim=this.node.getComponent(cc.Animation);
    }


    onBeginContact(contact, self, other)
    {
        if(other.node.name=='flow_left_end'||other.node.name=='flow_right_end'||other.node.name=='flow_up_end'||other.node.name=='flow_down_end'
        ||other.node.name=='flow_left'||other.node.name=='flow_right'||other.node.name=='flow_up'||other.node.name=='flow_down')
        {
            if(this.node.name=='move_block')this.scheduleOnce(this.block_destroy,0.25);
            else this.scheduleOnce(this.block_destroy,0.65);
            this.animstate=this.anim.play();
        }
    }

    block_destroy()
    {
        if(this.have_item==0)
        {
            var item;
            if(this.which_item==0)
            {
                item=cc.instantiate(this.add_num);
            }
            else if(this.which_item==1)
            {
                item=cc.instantiate(this.add_power);
            }
            else if(this.which_item==2)
            {
                item=cc.instantiate(this.add_speed);
            }
            item.x=this.node.x;
            item.y=this.node.y;
            item.parent=cc.find('Canvas/item');
        }
        this.node.destroy();
    }

    sendDataToServer(type:string, ...additionalData) {
        switch (type) {
            case "ITEM":
                this.mgr.ws.send(JSON.stringify({
                    type: "ITEM",
                    other_id: this.mgr.other_id,
                    item_gen: additionalData[0],
                    which_item:additionalData[1],
                }));
                break;
        
            default:
                break;
        }
    }

}
