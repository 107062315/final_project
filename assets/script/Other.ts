// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;


@ccclass
export default class Other extends cc.Component {

    // animation
    anim: cc.Animation = null;
    animateState = null;

    //other bubble power
    other_power:number=1;

    stuck:boolean=false;


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.anim = this.node.getComponent(cc.Animation);
        this.animateState = this.anim.play('player_idle_down');
    }

    start () {

    }

    // update (dt) {}

    playAnimation(newAnimateState: string) {
        if(this.animateState.name != newAnimateState) {
            this.animateState = this.anim.play(newAnimateState);
        }
    }

    onPreSolve(contact, self, other)
    {
        if(other.node.parent.name=='block')
        {
            if(this.stuck)contact.disabled=true;
            else contact.disabled=false;
        }
    }

    onBeginContact(contact, self, other)
    {
        if(other.node.name=='add_power')
        {
            this.other_power++;
        }
    }
}
