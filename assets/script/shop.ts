// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
import GameMgr from "./GameMgr";
@ccclass
export default class Shop extends cc.Component {

    @property(cc.Node)
    lobby_button: cc.Node = null;
    @property(cc.Node)
    skin_button: cc.Node = null;
    @property(cc.Node)
    pin_button: cc.Node = null;
    @property(cc.Node)
    fivepin_button: cc.Node = null;
    @property(cc.Node)
    moneyNode: cc.Node = null;
    @property(cc.Node)
    pinNode: cc.Node = null;
    @property(cc.Node)
    sellout: cc.Node = null;

    public money: number = null;
    public pin: number = null;
    public buyflag = 1;
    

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.lobby_button.on(cc.Node.EventType.MOUSE_DOWN, this.toLobby, this);
        this.skin_button.on(cc.Node.EventType.MOUSE_DOWN, this.buyskin, this);
        this.pin_button.on(cc.Node.EventType.MOUSE_DOWN, this.buypin, this);
        this.fivepin_button.on(cc.Node.EventType.MOUSE_DOWN, this.buy5pin, this);
        let email = cc.find("GameMgr").getComponent(GameMgr).email;
        firebase.database().ref(email).once('value', (snapshot) => {
            this.money = snapshot.val().money;
            this.pin = snapshot.val().pin;
            this.buyflag = snapshot.val().buyflag;
        });
        cc.find("sellout").active = false;
    }

    start () {

    }

    toLobby() {
        cc.director.loadScene('lobby');
    }

    buyskin(){
        if(this.money>=9999 && this.buyflag == 1){
            this.money -= 9999;
            this.buyflag = 0;
            cc.find("sellout").active = true;
        }
        let email = cc.find("GameMgr").getComponent(GameMgr).email;
        firebase.database().ref(email + '/money').set(this.money);
        firebase.database().ref(email + '/buyflag').set(this.buyflag);
    }

    buy5pin(){
        if(this.money>=450){
            this.money -= 450;
            this.pin += 5;
        }
        let email = cc.find("GameMgr").getComponent(GameMgr).email;
        firebase.database().ref(email + '/money').set(this.money);
        firebase.database().ref(email + '/pin').set(this.pin);
    }

    buypin(){
        if(this.money>=100){
            this.money -= 100;
            this.pin += 1;
        }
        let email = cc.find("GameMgr").getComponent(GameMgr).email;
        firebase.database().ref(email + '/money').set(this.money);
        firebase.database().ref(email + '/pin').set(this.pin);
    }

     update (dt) {
        let money_string = " "+this.money;
        let pin_string = " "+this.pin;
        cc.find("moneynode").getComponent(cc.Label).string = money_string;
        cc.find("pinnode").getComponent(cc.Label).string = pin_string;
        if(!this.buyflag)cc.find("sellout").active = true;
     }
}
