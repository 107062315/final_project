// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
const {ccclass, property} = cc._decorator;
import GameMgr from "./GameMgr";
@ccclass
export default class LogIn extends cc.Component {
    @property(cc.Node)
    login: cc.Node = null;
    @property(cc.Node)
    signin: cc.Node = null;
    @property(cc.Node)
    password: cc.Node = null;
    @property(cc.Node)
    email: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        this.login.on(cc.Node.EventType.MOUSE_DOWN,this.clickLogin,this);
        this.signin.on(cc.Node.EventType.MOUSE_DOWN, this.clickRegister, this);
    }
    start () {
    }
    changeVal(u_email){
        var email = '';
        for(var i in u_email)
        {
            if(u_email[i]=='.'||u_email[i]=='#'||u_email[i]=='$'||u_email[i]=='['||u_email[i]==']')email += '-';
            else email += u_email[i];
        }
        return email;
    }
    async clickLogin() {
        let user_email = this.email.getComponent(cc.EditBox).string;
        let user_pwd = this.password.getComponent(cc.EditBox).string;
        try {
            await firebase.auth().signInWithEmailAndPassword(user_email,user_pwd);
            cc.find('GameMgr').getComponent(GameMgr).email = this.changeVal(user_email);
            firebase.database().ref(this.changeVal(user_email)).once('value', function(snapshot){
                cc.find('GameMgr').getComponent(GameMgr).money = snapshot.val().money;
                cc.find('GameMgr').getComponent(GameMgr).pin = snapshot.val().pin;
                cc.find('GameMgr').getComponent(GameMgr).buyflag = snapshot.val().buyflag;
            });
            cc.director.loadScene('lobby');
        } catch (error) {
            alert(error.message);
        }
    }
    async clickRegister() {
        let user_email = this.email.getComponent(cc.EditBox).string;
        let user_pwd = this.password.getComponent(cc.EditBox).string;
        try {
            await firebase.auth().createUserWithEmailAndPassword(user_email,user_pwd);
            cc.find('GameMgr').getComponent(GameMgr).email = this.changeVal(user_email);
            cc.find('GameMgr').getComponent(GameMgr).money = 19999;
            cc.find('GameMgr').getComponent(GameMgr).pin = 0;
            cc.find('GameMgr').getComponent(GameMgr).buyflag = 1;
            firebase.database().ref(this.changeVal(user_email)).set({
                money:19999,
                pin:0,
                buyflag:1
            });
        } catch (error) {
            alert(error.message);
        }
    }
    // update (dt) {}
}