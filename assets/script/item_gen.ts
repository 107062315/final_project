// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
import GameMgr from "./GameMgr"

@ccclass
export default class item_gen extends cc.Component {

    block=null;
    item_gen=[];
    which_item=[];

    mgr: GameMgr = null;

    final_item_x=[];
    final_item_y=[];
    final_which_item=[];

    already_push:boolean=false;

    onLoad()
    {
        this.mgr = cc.find("GameMgr").getComponent(GameMgr);

        this.block=cc.find('Canvas/block').children;
        for(var i=0;i<this.block.length;i++)
        {
            if(this.block[i].name=='redblock'||this.block[i].name=='orangeblock'||this.block[i].name=='move_block')
            {
                var item_gen=Math.floor(Math.random()*2);
                var which_item=Math.floor(Math.random()*3);
                this.item_gen.push(item_gen);
                this.which_item.push(which_item);
            }
            else 
            {
                this.item_gen.push(-1);
                this.which_item.push(-1);
            }

        }

        
    }

    update()
    {
        this.sendDataToServer('ITEM',this.item_gen,this.which_item);
        if(this.already_push)return;
        if(cc.find('GameMgr').getComponent(GameMgr).have_item)
        {   
            for(var i=0;i<this.block.length;i++)
            {
                if(this.block[i].name=='redblock'||this.block[i].name=='orangeblock'||this.block[i].name=='move_block')
                {   
                    if(this.item_gen[i]==0&&cc.find('GameMgr').getComponent(GameMgr).item_gen[i]==0)
                    {
                        this.block[i].getComponent('block').have_item=0;
                        if(this.which_item[i]<=cc.find('GameMgr').getComponent(GameMgr).which_item[i])
                        this.block[i].getComponent('block').which_item=this.which_item[i];
                        else
                        this.block[i].getComponent('block').which_item=cc.find('GameMgr').getComponent(GameMgr).which_item[i];
                    }
                }
            }
            this.already_push=true;
        }
    }

    sendDataToServer(type:string, ...additionalData) {
        switch (type) {
            case "ITEM":
                this.mgr.ws.send(JSON.stringify({
                    type: "ITEM",
                    other_id: this.mgr.other_id,
                    item_gen:additionalData[0],
                    which_item:additionalData[1]
                }));
                break;
        
            default:
                break;
        }
    }

   
}
