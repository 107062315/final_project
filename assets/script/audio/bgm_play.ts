// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class bgm_play extends cc.Component {

    sound_script=null;
    sound=null;

   onLoad()
   {
       this.sound_script=cc.find('audio').getComponent('audio');
       this.sound=this.sound_script.audioClips;
   }

   start()
   {
       if(this.sound[1])
       {
            cc.audioEngine.playMusic(this.sound[0],false);
            this.scheduleOnce(function()
            {
                    cc.audioEngine.playMusic(this.sound[1],true);
            },1);
       }
       else
       {
            cc.audioEngine.playMusic(this.sound[0],true);
       }
   }
}
