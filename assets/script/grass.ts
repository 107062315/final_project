// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class grass extends cc.Component {

    onBeginContact(contact, self, other)
    {
        if(other.node.name=='player'||other.node.name=='other_prefab')
        {
            other.node.opacity=0;
        }
        if(other.node.name=='flow_left_end'||other.node.name=='flow_right_end'||other.node.name=='flow_up_end'||other.node.name=='flow_down_end'
        ||other.node.name=='flow_left'||other.node.name=='flow_right'||other.node.name=='flow_up'||other.node.name=='flow_down'||other.node.name=='flow_center')
        {
            this.node.destroy();
        }
    }
    onEndContact(contact, self, other)
    {
        if(other.node.name=='player'||other.node.name=='other_prefab')
        {
            other.node.opacity=255;
        }
    }
}
