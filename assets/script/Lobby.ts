// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

import GameMgr from "./GameMgr"

@ccclass
export default class Lobby extends cc.Component {


    @property(cc.Node)
    createRoom_button: cc.Node = null;
    @property(cc.Node)
    joinRoom_button: cc.Node = null;
    @property(cc.Node)
    shop_button: cc.Node = null;
    @property(cc.Node)
    roomID: cc.Node = null;

    @property(cc.Node)
    roomContainer: cc.Node = null;

    @property(cc.Prefab)
    room_prefab: cc.Prefab = null;

    GameMgr: GameMgr = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

        this.createRoom_button.on(cc.Node.EventType.MOUSE_DOWN, this.createRoom, this);
        this.joinRoom_button.on(cc.Node.EventType.MOUSE_DOWN, this.joinRoom, this);
        this.shop_button.on(cc.Node.EventType.MOUSE_DOWN, this.toShop, this);
        this.GameMgr = cc.find("GameMgr").getComponent(GameMgr);
        this.GameMgr.scene = "Lobby";
        this.GameMgr.isOwner = false;
        this.GameMgr.inRoom = false;
        this.GameMgr.other_id = -1;
        /*firebase.database().ref("rooms").on("child_added", (snapshot) => {
            var newRoom = cc.instantiate(this.room_prefab);
            newRoom.getChildByName("roomName").getComponent(cc.Label).string = snapshot.val().id;
            this.roomContainer.addChild(newRoom);
        });*/
    }

    start () {

    }

    //update (dt) {}

    createRoom() {
        //var newRoom = cc.instantiate(this.room_prefab);
        //this.roomContainer.addChild(newRoom);
        /*firebase.database().ref("rooms").push({
            id: this.GameMgr.cliend_id
        });*/
        this.GameMgr.inRoom = this.GameMgr.isOwner = true;
        this.GameMgr.isSingle = false;
        this.GameMgr.mode = "PVP";
        cc.director.loadScene('waitingroom');
    }

    toShop() {
        cc.director.loadScene('Shop');
    }

    joinRoom() {
        var roomNumber =  parseInt(this.roomID.getComponent(cc.EditBox).string, 10);
        if(!isNaN(roomNumber)) {
            this.GameMgr.ws.send(JSON.stringify({
                type: "JOIN_req",
                other_id: roomNumber,
                sponsor: this.GameMgr.cliend_id
            }));
        } 
    }

    boss1()
    {
        cc.director.loadScene('boss1');
    }
}