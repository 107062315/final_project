// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class block_in_boss extends cc.Component {

    @property(cc.Prefab)
    add_num:cc.Prefab=null;
    @property(cc.Prefab)
    add_power:cc.Prefab=null;
    @property(cc.Prefab)
    add_speed:cc.Prefab=null;

    anim=null;

    onLoad()
    {
        this.anim=this.node.getComponent(cc.Animation);
    }

    onBeginContact(contact, self, other)
    {
        if(other.node.name=='flow_left_end'||other.node.name=='flow_right_end'||other.node.name=='flow_up_end'||other.node.name=='flow_down_end'
        ||other.node.name=='flow_left'||other.node.name=='flow_right'||other.node.name=='flow_up'||other.node.name=='flow_down')
        {
            this.scheduleOnce(this.block_destroy,0.65);
            this.anim.play();
        }
    }

    block_destroy()
    {
        var item_gen=Math.floor(Math.random()*2);
        if(item_gen==0)
        {
            var which_item=Math.floor(Math.random()*3);
            var item;
            if(which_item==0)
            {
                item=cc.instantiate(this.add_num);
            }
            else if(which_item==1)
            {
                item=cc.instantiate(this.add_power);
            }
            else if(which_item==2)
            {
                item=cc.instantiate(this.add_speed);
            }
            item.x=this.node.x;
            item.y=this.node.y;
            item.parent=cc.find('Canvas/item');
        }
        this.node.destroy();
    }
}
