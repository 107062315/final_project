// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class bubble_gen extends cc.Component {

    //水球
    @property(cc.Prefab)
    bubble:cc.Prefab=null;

    create_bubble=null;

    //地圖格子座標
    private map_x=[];
    private map_y=[];

    private map_x_placed=[];
    private map_y_placed=[];

    private attacking:boolean=false;

    private begin_attack:boolean=true;


    onLoad()
    {
        //map
        for(var i=0,ini_x=140;i<15;i++)
        {
            this.map_x.push(ini_x);
            ini_x-=40;
            this.map_x_placed.push(false);
        }
        for(var j=0,ini_y=-260;j<13;j++)
        {
            this.map_y.push(ini_y);
            ini_y+=40;
            this.map_y_placed.push(false);
        }
    }

    start()
    {
        this.scheduleOnce(this.attack1.bind(this),5);
        this.scheduleOnce(this.attack2.bind(this),8);
        this.scheduleOnce(this.attack3.bind(this),9);
        this.scheduleOnce(function()
        {
            this.begin_attack=false;
        },10);
    }

    update()
    {
        if(cc.find('Canvas/boss').getComponent('boss').boss_die)return;
        
        if(this.begin_attack)return;
        if(!this.attacking)
        {
            this.attacking=true;
            var mode=Math.floor(Math.random()*4);
            if(mode==0)
            {
                this.scheduleOnce(this.attack2.bind(this),5);
                this.scheduleOnce(this.attack3.bind(this),6);
            }
            else if(mode==1||mode==2||mode==3)
            {
                this.scheduleOnce(this.random_attack.bind(this),5);
            }
            this.scheduleOnce(function()
            {
                this.attacking=false;
            },8)
        }
    }

    attack1()
    {
        var bubble_x=[100,60,100,140];
        var bubble_y=[-180,-220,-260,-220];
        for(var i=0;i<4;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(50,170),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }

        bubble_x=[100,60,100,140];
        bubble_y=[140,180,220,180];
        for(var i=0;i<4;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(50,-210),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }

        bubble_x=[-380,-420,-380,-340];
        bubble_y=[140,180,220,180];
        for(var i=0;i<4;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(-330,-210),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }

        bubble_x=[-380,-420,-380,-340];
        bubble_y=[-260,-220,-180,-220];
        for(var i=0;i<4;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(-330,170),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }
    }

    attack2()
    {
        if(cc.find('Canvas/boss').getComponent('boss').boss_die)return;

        var bubble_x=[-20,20,60,100];
        var bubble_y=[-140,-180,-220,-260]
        for(var i=0;i<4;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(50,-210),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }

        bubble_x=[-260,-300,-340,-380];
        bubble_y=[100,140,180,220];
        for(var i=0;i<4;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(-330,170),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }
    }

    attack3()
    {
        if(cc.find('Canvas/boss').getComponent('boss').boss_die)return;

        var bubble_x=[-260,-300,-340,-380,-420,-220,-180,-140,-100];
        var bubble_y=[-100,-60,-20,20,60,-140,-180,-220,-260];
        for(var i=0;i<9;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(50,-210),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }

        bubble_x=[-60,-100,-140,-180,-20,20,60,100,140];
        bubble_y=[100,140,180,220,60,20,-20,-60,-100];

        for(var i=0;i<9;i++)
        {
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);

            var bezier=[cc.v2(-140,-20),cc.v2(-330,170),cc.v2(bubble_x[i],bubble_y[i])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }
    }

    random_attack()
    {
        if(cc.find('Canvas/boss').getComponent('boss').boss_die)return;

        for(var i=0;i<15;i++)
        {
            var random_x=Math.floor(Math.random()*15);
            var random_y=Math.floor(Math.random()*13);
            while((this.map_x_placed[random_x]==true&&this.map_y_placed[random_y]==true)||((5<=random_x&&random_x<=9)&&(4<=random_y&&random_y<=8)))
            {
                var random_x=Math.floor(Math.random()*15);
                var random_y=Math.floor(Math.random()*13);
            }
            this.map_x_placed[random_x]=true;
            this.map_y_placed[random_y]=true;
            var bubble=cc.instantiate(this.bubble);
            bubble.x=-140;
            bubble.y=-20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier=[cc.v2(-140,-20),cc.v2(-330,170),cc.v2(this.map_x[random_x],this.map_y[random_y])];
            var action=cc.bezierTo(2,bezier);
            bubble.runAction(action);
        }
        for(var i=0;i<15;i++)
        {
            this.map_x_placed[i]=false;
        }
        for(var i=0;i<13;i++)
        {
            this.map_y_placed[i]=false;
        }
    }

    
}
