// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class boss extends cc.Component {

    //music
    sound_script=null;
    sound=null;

    anim=null;
    animstate=null;

    HP:number=3;
    HP_width:number;

    boss_die:boolean=false;

    been_attacked:boolean=false;

    onLoad()
    {
         //music
         this.sound_script=cc.find('audio').getComponent('audio');
         this.sound=this.sound_script.audioClips;
 

        this.anim=this.node.getComponent(cc.Animation);
        this.animstate=this.anim.play();
        this.HP_width=cc.find('Canvas/boss_HPframe/boss_HP').width;
    }

    update()
    {
        if(this.boss_die)return;
        if(this.HP==0)
        {
            cc.audioEngine.playEffect(this.sound[3],false);
            this.boss_die=true;
            this.anim.play('boss_die');
            this.scheduleOnce(function()
            {
                cc.audioEngine.playEffect(this.sound[4],false);
            },2)
            this.scheduleOnce(this.win,4);
            this.scheduleOnce(function()
            {
                cc.director.loadScene('lobby');
            },8);
        }
        if(!this.animstate.isPlaying&&this.HP!=0)
        {
            this.animstate=this.anim.play();
        }
    }
   
    onBeginContact(contact, self, other)
    {
        if(other.node.name=='flow_left_end'||other.node.name=='flow_right_end'||other.node.name=='flow_up_end'||other.node.name=='flow_down_end'
        ||other.node.name=='flow_left'||other.node.name=='flow_right'||other.node.name=='flow_up'||other.node.name=='flow_down')
        {
            if(this.been_attacked)return;
            this.animstate=this.anim.play('boss_hurt');
            if(this.HP!=0)
            {
                this.HP-=1;
                this.been_attacked=true;
                cc.find('Canvas/boss_HPframe/boss_HP').width-=this.HP_width*1/3;
                cc.find('Canvas/boss_HPframe/boss_HP').x-=(this.HP_width*(1/3)*(1/2));
            }
        }
    }

    onEndContact(contact, self, other)
    {
        this.been_attacked=false;
    }

    win()
    {
        cc.audioEngine.playMusic(this.sound[6],false);
        var action=cc.moveBy(3,0,-300);
        cc.find('Canvas/win').runAction(action);
        this.node.opacity=0;
    }
}
