// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import player from "./player"
import Other from './Other'
import Waitingroom from "./waiting_room";

@ccclass
export  default class GameMgr extends cc.Component {


    //music
    sound_script=null;
    sound=null;

    // player Manager
    player_node: cc.Node = null;
    player: player = null;

    // WebSocket Manager
    ws: WebSocket = null;
    isConnected: boolean = false;
    cliend_id: number = -1;
    other_id: number = -1;

    // Scene Manager
    scene: string = null;

    public email: string = "";

    public money: number = 19999;

    public pin: number = 0;

    public buyflag: number = 0;

    // Other Player Manager
    @property(cc.Prefab)
    other_prefab: cc.Prefab = null;
    other_node: cc.Node = null;
    Other: Other = null;
    @property(cc.Prefab)
    other_in_bubble:cc.Prefab=null;
    other_in_bubble_node:cc.Node;

    // other bubble Manager
    @property(cc.Prefab)
    otherBubble_prefab: cc.Prefab = null;

    //item manager
    item_gen=[];
    which_item=[];
    have_item:boolean=false;


    // Room Manager
    isOwner: boolean = false;
    inRoom: boolean = false;
    isSingle: boolean = false;
    mode: string = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
            
        cc.game.addPersistRootNode(this.node);
        this.initConnect();
        this.scene = cc.director.getScene().name;
    }

    start() {
        // merge test
        console.log('merge and deploy test');
    }

    update(dt) {
        if(cc.director.getScene().name=='game')
        {
            //music
            this.sound_script=cc.find('audio').getComponent('audio');
            this.sound=this.sound_script.audioClips;
        }
    }

    initConnect() {
        this.ws = new WebSocket('wss://140.114.206.100:8888/');

        this.ws.onopen = (event) => {
            this.isConnected = true;
        }

        this.ws.onmessage = (event) => {
            var msg = JSON.parse(event.data);
            switch (msg.type) {
                case "CLIENT_ID":
                    this.cliend_id = msg.client_id;
                    console.log(this.cliend_id);
                    break;
                case "OTHER_DATA":
                    if (this.scene == 'game') {
                        if (this.other_node == null) {
                            this.other_node = cc.instantiate(this.other_prefab);
                            this.Other = this.other_node.getComponent(Other);
                            this.other_node.parent = cc.find("Canvas");
                        }
                        this.other_node.setPosition(msg.x, msg.y);
                        this.Other.playAnimation(msg.animateState);
                    }
                    break;
                case "OTHER_JOIN":
                    this.other_id = msg.other_id;
                    break;
                case "PLACE_BUBBLE":
                    this.placeBubble(msg.x, msg.y);
                    break;

                case "PLAYER_IN_BUBBLE":
                    this.place_other_in_bubble(msg.x,msg.y);
                    break;

                case "PLAYER_DIE":
                    this.other_die_soon();
                    break;

                case "ITEM":
                    this.item_gen=msg.item_gen;
                    this.which_item=msg.which_item;
                    this.have_item=true;
                case "JOIN_req":
                    this.joinReqHandler(msg);
                    break;
                
                case "JOIN_res":
                    this.joinResHandler(msg);
                    break;

                case "REBORN":
                    this.other_reborn();
                    break

                
                case "ERROR":
                    console.log("error: ",msg.code);
                    console.log(msg.code);
                    break;

                case "GAME_MODE":
                    this.gameModeHandler(msg);
                    break;

                case "GAME_START":
                    if(this.mode=='PVP')cc.director.loadScene('game');
                    break;
                default:
                    break;
            }
        }
    }

    placeBubble(x, y) {
        var otherBubble =  cc.instantiate(this.otherBubble_prefab);
        otherBubble.parent = cc.find("Canvas/other_bubble");
        otherBubble.setPosition(x,y);
    }

    place_other_in_bubble(x,y)
    {
        cc.audioEngine.playEffect(this.sound[3],false);
        this.other_node.getComponent('Other').stuck=true;
        cc.find('Canvas/other_prefab').opacity=255;
        this.other_in_bubble_node=cc.instantiate(this.other_in_bubble);
        this.other_in_bubble_node.x=x;
        this.other_in_bubble_node.y=y;

        this.scheduleOnce(this.change_scene,9)
        this.scheduleOnce(this.win,5)
        this.scheduleOnce(this.explode,4.5)
        this.scheduleOnce(this.other_explode_sound,4.5)

        var bubble_move=cc.repeatForever(cc.sequence(cc.moveBy(0.7,0,-10),cc.moveBy(0.7,0,10)));
        this.other_in_bubble_node.runAction(bubble_move);

        var player_move=cc.repeatForever(cc.sequence(cc.moveBy(0.7,0,-10),cc.moveBy(0.7,0,10)));
        cc.find('Canvas/other_prefab').runAction(player_move);

        cc.find('Canvas').addChild(this.other_in_bubble_node);

        var action=cc.fadeOut(4);
        cc.find('Canvas/other_prefab').runAction(action);

        cc.find('Canvas/other_prefab').parent= this.other_in_bubble_node;

    }

    win()
    {
        cc.audioEngine.playMusic(this.sound[8],false);
        this.other_in_bubble_node.destroy();
        var action=cc.moveBy(3,0,-300);
        cc.find('Canvas/win').runAction(action);
    }

    explode()
    {
        this.other_in_bubble_node.getComponent(cc.Animation).play();
    }

    other_explode_sound()
    {
        cc.audioEngine.playEffect(this.sound[4],false);
    }

    other_die_soon()
    {
        this.unschedule(this.win);
        this.unschedule(this.explode);
        this.scheduleOnce(this.win,1);
        this.scheduleOnce(this.explode,0.5);
    }

    change_scene()
    {
        cc.director.loadScene('lobby');
    }

    other_reborn()
    {
        this.other_node.stopAllActions();
        this.other_node.parent.stopAllActions();
        this.unschedule(this.change_scene);
        this.unschedule(this.win);
        this.unschedule(this.explode);
        this.unschedule(this.other_explode_sound);
        this.explode();
        this.scheduleOnce(function()
        {
            this.other_node.getComponent('Other').stuck=false;
            var x_before_reborn=this.other_node.x;
            var y_before_reborn=this.other_node.y;
            this.other_node.opacity=255;
            var ini_parent= this.other_node.parent;
            this.other_node.parent=cc.find('Canvas');
            this.other_node.x=x_before_reborn;
            this.other_node.y=y_before_reborn;
            ini_parent.destroy();
        },0.3)
    }

    joinReqHandler(msg) {
        console.log(this.other_id);
        if( this.other_id == -1 && this.inRoom && this.isOwner && !this.isSingle) { // 沒其他玩家且是房間創建者同時也允許多人
            this.other_id = msg.sponsor;
            this.ws.send(JSON.stringify({
                type: "JOIN_res",
                res: "accept",
                sponsor: msg.sponsor,
                other_id: this.cliend_id,
                mode: this.mode
            }));
            cc.find("P2").active = true;
        } else {
            this.ws.send(JSON.stringify({
                type: "JOIN_res",
                res: "reject",
                sponsor: msg.sponsor,
                other_id: this.cliend_id
            }));
        }
    }

    joinResHandler(msg) {
        if( msg.res == "reject") {
            alert('The room rejects your request.');
            console.log("reject");
        } else {
            this.other_id = msg.other_id;
            console.log(this.other_id);
            this.inRoom = true;
            this.scene = "waitingroom";
            this.mode = msg.mode;
            cc.director.loadScene('waitingroom');
        }
    }

    gameModeHandler(msg) {
        if(this.other_id == -1) return;

        if(msg.mode == "BOSS") {
            cc.find("Canvas/boss_bg").active = true;
        }else {
            cc.find("Canvas/boss_bg").active = false;
        }
    }
}
