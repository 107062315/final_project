// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import GameMgr from "./GameMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Waitingroom extends cc.Component {

    @property(cc.Node)
    lobby_button: cc.Node = null;
    @property(cc.Node)
    PVP_boss_button: cc.Node = null;//boss or PVP
    @property(cc.Node)
    mode_button: cc.Node = null;//one or two players
    @property(cc.Node)
    GO_button: cc.Node = null;
    @property(cc.Node)
    boss_scene: cc.Node = null;
    @property(cc.Node)
    boss_label: cc.Node = null;
    @property(cc.Node)
    PVP_label: cc.Node = null;
    @property(cc.Node)
    mode_btn_img: cc.Node = null;
    @property(cc.Node)
    one_label: cc.Node = null;
    @property(cc.Node)
    two_label: cc.Node = null;
    @property(cc.Node)
    waitfriend_off: cc.Node = null;

    @property(cc.Label)
    roomID_Label: cc.Label = null;

    public Bossflag = false;

    GameMgr: GameMgr = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.GameMgr = cc.find("GameMgr").getComponent(GameMgr);
        
        this.lobby_button.on(cc.Node.EventType.MOUSE_DOWN, this.toLobby, this);
        if (this.GameMgr.isOwner) {
            this.PVP_boss_button.on(cc.Node.EventType.MOUSE_DOWN, this.clickPVPorboss, this);
            this.mode_button.on(cc.Node.EventType.MOUSE_DOWN, this.clickmode, this);
            this.GO_button.on(cc.Node.EventType.MOUSE_DOWN, this.gameStart, this);
            this.roomID_Label.string = this.GameMgr.cliend_id.toString(10);
        } else {
            this.PVP_boss_button.active = false;
            this.GO_button.active = false;
            this.roomID_Label.node.active = false;
            cc.find("P2").active = true;
        }
        if(this.GameMgr.mode != "BOSS")cc.find("Canvas/boss_bg").active = false;
        cc.find("Canvas/PVP_boss_btn_img/boss Label").active = false;
        cc.find("Canvas/mode_btn_img").active = false;
        cc.find("Canvas/mode_btn_img/two Label").active = false;
        cc.find("Canvas/waitfriend_off").active = false;
    }

    start() {

    }

    toLobby() {
        cc.director.loadScene('lobby');
    }

    clickPVPorboss() {
        if (this.GameMgr.isOwner == false) return;

        this.Bossflag = !this.Bossflag;
        if (this.GameMgr.other_id != -1) {
            cc.find("Canvas/mode_btn_img/one Label").active = false;
            cc.find("Canvas/mode_btn_img/two Label").active = true;
            cc.find("Canvas/waitfriend_off").active = false;
        }
        if (this.Bossflag) {
            this.GameMgr.mode = "BOSS";
            cc.find("Canvas/boss_bg").active = true;
            cc.find("Canvas/PVP_boss_btn_img/boss Label").active = true;
            cc.find("Canvas/PVP_boss_btn_img/PVP Label").active = false;
            cc.find("Canvas/mode_btn_img").active = true;
            if (!this.GameMgr.isSingle) {
                cc.find("Canvas/waitfriend_off").active = false;
                this.GameMgr.ws.send(JSON.stringify({
                    type: "GAME_MODE",
                    mode: "BOSS",
                    other_id: this.GameMgr.other_id
                }));
            }
            else {
                cc.find("Canvas/waitfriend_off").active = true;
            }
        }
        else {
            this.GameMgr.mode = "PVP";
            cc.find("Canvas/boss_bg").active = false;
            cc.find("Canvas/PVP_boss_btn_img/boss Label").active = false;
            cc.find("Canvas/PVP_boss_btn_img/PVP Label").active = true;
            cc.find("Canvas/mode_btn_img").active = false;
            cc.find("Canvas/waitfriend_off").active = false;
            this.GameMgr.isSingle = false;
            this.GameMgr.ws.send(JSON.stringify({
                type: "GAME_MODE",
                mode: "PVP",
                other_id: this.GameMgr.other_id
            }));
        }

    }

    clickmode() {
        if (this.GameMgr.isOwner == false) return;
        if (this.GameMgr.other_id != -1) {
            return;
        }
        this.GameMgr.isSingle = !this.GameMgr.isSingle;
        if (!this.GameMgr.isSingle) {
            cc.find("Canvas/mode_btn_img/one Label").active = false;
            cc.find("Canvas/mode_btn_img/two Label").active = true;
            cc.find("Canvas/waitfriend_off").active = false;
        }
        else {
            cc.find("Canvas/mode_btn_img/one Label").active = true;
            cc.find("Canvas/mode_btn_img/two Label").active = false;
            cc.find("Canvas/waitfriend_off").active = true;
        }
    }

    update(dt) {
        if (!this.GameMgr.isSingle) {
            cc.find("Canvas/mode_btn_img/one Label").active = false;
            cc.find("Canvas/mode_btn_img/two Label").active = true;
            cc.find("Canvas/waitfriend_off").active = false;
        }
        else {
            cc.find("Canvas/mode_btn_img/one Label").active = true;
            cc.find("Canvas/mode_btn_img/two Label").active = false;
            cc.find("Canvas/waitfriend_off").active = true;
        }
    }

    gameStart() {
        if (this.GameMgr.isSingle && this.Bossflag) {
            cc.director.loadScene("boss1");
        } else if (!this.GameMgr.isSingle && this.Bossflag) {
            if (this.GameMgr.other_id != -1) {
                this.GameMgr.ws.send(JSON.stringify({
                    type: "GAME_START",
                    other_id: this.GameMgr.other_id
                }));
            }
        } else if (!this.Bossflag) {
            if (this.GameMgr.other_id != -1) {
                this.GameMgr.ws.send(JSON.stringify({
                    type: "GAME_START",
                    other_id: this.GameMgr.other_id
                }));
                cc.director.loadScene('game');
            }
        }
    }
}
