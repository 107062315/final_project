// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class bubble extends cc.Component {

    @property(cc.Prefab)
    flow_center:cc.Prefab=null;
    @property(cc.Prefab)
    flow_down:cc.Prefab=null;
    @property(cc.Prefab)
    flow_down_end:cc.Prefab=null;
    @property(cc.Prefab)
    flow_up:cc.Prefab=null;
    @property(cc.Prefab)
    flow_up_end:cc.Prefab=null;
    @property(cc.Prefab)
    flow_left:cc.Prefab=null;
    @property(cc.Prefab)
    flow_left_end:cc.Prefab=null;
    @property(cc.Prefab)
    flow_right:cc.Prefab=null;
    @property(cc.Prefab)
    flow_right_end:cc.Prefab=null;

    out_of_bubble:boolean=false;
    other_out_of_bubble:boolean=false;

    player=null;
    other_player=null;

    //水球威力
    private power:number;

    //block位置
    private block;

    //水柱遇到障礙物停止
    private stop:boolean=false;

    //music
    sound_script=null;
    sound=null;
    

    onLoad()
    {
        //music
        this.sound_script=cc.find('audio').getComponent('audio');
        this.sound=this.sound_script.audioClips;

        this.player=cc.find('Canvas/player');
        if(this.node.parent.name=='other_bubble')this.power=cc.find('Canvas/other_prefab').getComponent('Other').other_power;
        else this.power=cc.find('Canvas/player').getComponent('player').bubble_power;
        this.block=cc.find('Canvas/block').children;
        var action=cc.repeatForever(cc.sequence(cc.scaleBy(0.25,1,0.8),cc.scaleBy(0.25,1,1.25)));
        this.node.runAction(action);
    }

    start()
    {
        this.other_player=cc.find('Canvas/other_prefab');
        this.scheduleOnce(this.explode,1.5);
    }

    update()
    {
        if(this.player.x<this.node.x-30||this.player.x>this.node.x+30||this.player.y<this.node.y-30||this.player.y>this.node.y+30)
        this.out_of_bubble=true;
        if(this.other_player.x<this.node.x-30||this.other_player.x>this.node.x+30||this.other_player.y<this.node.y-30||this.other_player.y>this.node.y+30)
        this.other_out_of_bubble=true;
    }

    onBeginContact(contact, self, other)
    {
        if(other.node.name=='player')
        {
            if(this.out_of_bubble)contact.disabled=false;
            else contact.disabled=true;
        }
        if(other.node.name == 'other_prefab')
        {
            if(this.other_out_of_bubble)contact.disabled=false;
            else contact.disabled=true;
        }
        if(other.node.name=='flow_left_end'||other.node.name=='flow_right_end'||other.node.name=='flow_up_end'||other.node.name=='flow_down_end'
        ||other.node.name=='flow_left'||other.node.name=='flow_right'||other.node.name=='flow_up'||other.node.name=='flow_down')
        {
            this.unschedule(this.explode);
            this.explode();            
        }
    }
    

    explode()
    {
        cc.audioEngine.playEffect(this.sound[5],false);

        var flow_center=cc.instantiate(this.flow_center);
        flow_center.x=this.node.x;
        flow_center.y=this.node.y;
        cc.find('Canvas/bubble_explode').addChild(flow_center);

        //左水柱
        for(var i=1;i<=this.power;i++)
        {
            if(i==this.power)var flow_left=cc.instantiate(this.flow_left_end);
            else var flow_left=cc.instantiate(this.flow_left);
            flow_left.x=this.node.x-i*40;
            flow_left.y=this.node.y;
            if(flow_left.x>=-430)
            cc.find('Canvas/bubble_explode').addChild(flow_left);

            //水柱不能超出障礙物
            for(var j=0;j<this.block.length;j++)
            {
                if(this.block[j].x==flow_left.x&&this.block[j].y==flow_left.y)
                {
                    this.stop=true;
                    break;
                }
            }
            if(this.stop)
            {
                this.stop=false;
                break;
            }
        }
        //右水柱
        for(var i=1;i<=this.power;i++)
        {
            if(i==this.power)var flow_right=cc.instantiate(this.flow_right_end);
            else var flow_right=cc.instantiate(this.flow_right);
            flow_right.x=this.node.x+i*40;
            flow_right.y=this.node.y;
            if(flow_right.x<=160)
            cc.find('Canvas/bubble_explode').addChild(flow_right);

            //水柱不能超出障礙物
            for(var j=0;j<this.block.length;j++)
            {
                if(this.block[j].x==flow_right.x&&this.block[j].y==flow_right.y)
                {
                    this.stop=true;
                    break;
                }
            }
            if(this.stop)
            {
                this.stop=false;
                break;
            }
        }
        //上水柱
        for(var i=1;i<=this.power;i++)
        {
            if(i==this.power)var flow_up=cc.instantiate(this.flow_up_end);
            else var flow_up=cc.instantiate(this.flow_up);
            flow_up.x=this.node.x;
            flow_up.y=this.node.y+i*40;
            if(flow_up.y<=220)
            cc.find('Canvas/bubble_explode').addChild(flow_up);

            //水柱不能超出障礙物
            for(var j=0;j<this.block.length;j++)
            {
                if(this.block[j].x==flow_up.x&&this.block[j].y==flow_up.y)
                {
                    this.stop=true;
                    break;
                }
            }
            if(this.stop)
            {
                this.stop=false;
                break;
            }
        }
        //下水柱
        for(var i=1;i<=this.power;i++)
        {
            if(i==this.power)var flow_down=cc.instantiate(this.flow_down_end);
            else var flow_down=cc.instantiate(this.flow_down);
            flow_down.x=this.node.x;
            flow_down.y=this.node.y-i*40;
            if(flow_down.y>=-260)
            cc.find('Canvas/bubble_explode').addChild(flow_down);

            //水柱不能超出障礙物
            for(var j=0;j<this.block.length;j++)
            {
                if(this.block[j].x==flow_down.x&&this.block[j].y==flow_down.y)
                {
                    this.stop=true;
                    break;
                }
            }
            if(this.stop)
            {
                this.stop=false;
                break;
            }
        }

        this.node.destroy();
    }
}
