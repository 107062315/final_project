(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/GameMgr.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '2f1edwuCpJD26pUDtbZapn7', 'GameMgr', __filename);
// script/GameMgr.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Other_1 = require("./Other");
var GameMgr = /** @class */ (function (_super) {
    __extends(GameMgr, _super);
    function GameMgr() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //music
        _this.sound_script = null;
        _this.sound = null;
        // player Manager
        _this.player_node = null;
        _this.player = null;
        // WebSocket Manager
        _this.ws = null;
        _this.isConnected = false;
        _this.cliend_id = -1;
        _this.other_id = -1;
        // Scene Manager
        _this.scene = null;
        _this.email = "";
        _this.money = 19999;
        _this.pin = 0;
        _this.buyflag = 0;
        // Other Player Manager
        _this.other_prefab = null;
        _this.other_node = null;
        _this.Other = null;
        _this.other_in_bubble = null;
        // other bubble Manager
        _this.otherBubble_prefab = null;
        //item manager
        _this.item_gen = [];
        _this.which_item = [];
        _this.have_item = false;
        // Room Manager
        _this.isOwner = false;
        _this.inRoom = false;
        _this.isSingle = false;
        _this.mode = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    GameMgr.prototype.onLoad = function () {
        cc.game.addPersistRootNode(this.node);
        this.initConnect();
        this.scene = cc.director.getScene().name;
    };
    GameMgr.prototype.start = function () {
        // merge test
        console.log('merge and deploy test');
    };
    GameMgr.prototype.update = function (dt) {
        if (cc.director.getScene().name == 'game') {
            //music
            this.sound_script = cc.find('audio').getComponent('audio');
            this.sound = this.sound_script.audioClips;
        }
    };
    GameMgr.prototype.initConnect = function () {
        var _this = this;
        this.ws = new WebSocket('wss://140.114.206.100:8888/');
        this.ws.onopen = function (event) {
            _this.isConnected = true;
        };
        this.ws.onmessage = function (event) {
            var msg = JSON.parse(event.data);
            switch (msg.type) {
                case "CLIENT_ID":
                    _this.cliend_id = msg.client_id;
                    console.log(_this.cliend_id);
                    break;
                case "OTHER_DATA":
                    if (_this.scene == 'game') {
                        if (_this.other_node == null) {
                            _this.other_node = cc.instantiate(_this.other_prefab);
                            _this.Other = _this.other_node.getComponent(Other_1.default);
                            _this.other_node.parent = cc.find("Canvas");
                        }
                        _this.other_node.setPosition(msg.x, msg.y);
                        _this.Other.playAnimation(msg.animateState);
                    }
                    break;
                case "OTHER_JOIN":
                    _this.other_id = msg.other_id;
                    break;
                case "PLACE_BUBBLE":
                    _this.placeBubble(msg.x, msg.y);
                    break;
                case "PLAYER_IN_BUBBLE":
                    _this.place_other_in_bubble(msg.x, msg.y);
                    break;
                case "PLAYER_DIE":
                    _this.other_die_soon();
                    break;
                case "ITEM":
                    _this.item_gen = msg.item_gen;
                    _this.which_item = msg.which_item;
                    _this.have_item = true;
                case "JOIN_req":
                    _this.joinReqHandler(msg);
                    break;
                case "JOIN_res":
                    _this.joinResHandler(msg);
                    break;
                case "REBORN":
                    _this.other_reborn();
                    break;
                case "ERROR":
                    console.log("error: ", msg.code);
                    console.log(msg.code);
                    break;
                case "GAME_MODE":
                    _this.gameModeHandler(msg);
                    break;
                case "GAME_START":
                    if (_this.mode == 'PVP')
                        cc.director.loadScene('game');
                    break;
                default:
                    break;
            }
        };
    };
    GameMgr.prototype.placeBubble = function (x, y) {
        var otherBubble = cc.instantiate(this.otherBubble_prefab);
        otherBubble.parent = cc.find("Canvas/other_bubble");
        otherBubble.setPosition(x, y);
    };
    GameMgr.prototype.place_other_in_bubble = function (x, y) {
        cc.audioEngine.playEffect(this.sound[3], false);
        this.other_node.getComponent('Other').stuck = true;
        cc.find('Canvas/other_prefab').opacity = 255;
        this.other_in_bubble_node = cc.instantiate(this.other_in_bubble);
        this.other_in_bubble_node.x = x;
        this.other_in_bubble_node.y = y;
        this.scheduleOnce(this.change_scene, 9);
        this.scheduleOnce(this.win, 5);
        this.scheduleOnce(this.explode, 4.5);
        this.scheduleOnce(this.other_explode_sound, 4.5);
        var bubble_move = cc.repeatForever(cc.sequence(cc.moveBy(0.7, 0, -10), cc.moveBy(0.7, 0, 10)));
        this.other_in_bubble_node.runAction(bubble_move);
        var player_move = cc.repeatForever(cc.sequence(cc.moveBy(0.7, 0, -10), cc.moveBy(0.7, 0, 10)));
        cc.find('Canvas/other_prefab').runAction(player_move);
        cc.find('Canvas').addChild(this.other_in_bubble_node);
        var action = cc.fadeOut(4);
        cc.find('Canvas/other_prefab').runAction(action);
        cc.find('Canvas/other_prefab').parent = this.other_in_bubble_node;
    };
    GameMgr.prototype.win = function () {
        cc.audioEngine.playMusic(this.sound[8], false);
        this.other_in_bubble_node.destroy();
        var action = cc.moveBy(3, 0, -300);
        cc.find('Canvas/win').runAction(action);
    };
    GameMgr.prototype.explode = function () {
        this.other_in_bubble_node.getComponent(cc.Animation).play();
    };
    GameMgr.prototype.other_explode_sound = function () {
        cc.audioEngine.playEffect(this.sound[4], false);
    };
    GameMgr.prototype.other_die_soon = function () {
        this.unschedule(this.win);
        this.unschedule(this.explode);
        this.scheduleOnce(this.win, 1);
        this.scheduleOnce(this.explode, 0.5);
    };
    GameMgr.prototype.change_scene = function () {
        cc.director.loadScene('lobby');
    };
    GameMgr.prototype.other_reborn = function () {
        this.other_node.stopAllActions();
        this.other_node.parent.stopAllActions();
        this.unschedule(this.change_scene);
        this.unschedule(this.win);
        this.unschedule(this.explode);
        this.unschedule(this.other_explode_sound);
        this.explode();
        this.scheduleOnce(function () {
            this.other_node.getComponent('Other').stuck = false;
            var x_before_reborn = this.other_node.x;
            var y_before_reborn = this.other_node.y;
            this.other_node.opacity = 255;
            var ini_parent = this.other_node.parent;
            this.other_node.parent = cc.find('Canvas');
            this.other_node.x = x_before_reborn;
            this.other_node.y = y_before_reborn;
            ini_parent.destroy();
        }, 0.3);
    };
    GameMgr.prototype.joinReqHandler = function (msg) {
        console.log(this.other_id);
        if (this.other_id == -1 && this.inRoom && this.isOwner && !this.isSingle) { // 沒其他玩家且是房間創建者同時也允許多人
            this.other_id = msg.sponsor;
            this.ws.send(JSON.stringify({
                type: "JOIN_res",
                res: "accept",
                sponsor: msg.sponsor,
                other_id: this.cliend_id,
                mode: this.mode
            }));
            cc.find("P2").active = true;
        }
        else {
            this.ws.send(JSON.stringify({
                type: "JOIN_res",
                res: "reject",
                sponsor: msg.sponsor,
                other_id: this.cliend_id
            }));
        }
    };
    GameMgr.prototype.joinResHandler = function (msg) {
        if (msg.res == "reject") {
            alert('The room rejects your request.');
            console.log("reject");
        }
        else {
            this.other_id = msg.other_id;
            console.log(this.other_id);
            this.inRoom = true;
            this.scene = "waitingroom";
            this.mode = msg.mode;
            cc.director.loadScene('waitingroom');
        }
    };
    GameMgr.prototype.gameModeHandler = function (msg) {
        if (this.other_id == -1)
            return;
        if (msg.mode == "BOSS") {
            cc.find("Canvas/boss_bg").active = true;
        }
        else {
            cc.find("Canvas/boss_bg").active = false;
        }
    };
    __decorate([
        property(cc.Prefab)
    ], GameMgr.prototype, "other_prefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], GameMgr.prototype, "other_in_bubble", void 0);
    __decorate([
        property(cc.Prefab)
    ], GameMgr.prototype, "otherBubble_prefab", void 0);
    GameMgr = __decorate([
        ccclass
    ], GameMgr);
    return GameMgr;
}(cc.Component));
exports.default = GameMgr;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=GameMgr.js.map
        