(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/boss1/bubble_gen.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '0d98bW0FNJKc5lZM/6G9VG9', 'bubble_gen', __filename);
// script/boss1/bubble_gen.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var bubble_gen = /** @class */ (function (_super) {
    __extends(bubble_gen, _super);
    function bubble_gen() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //水球
        _this.bubble = null;
        _this.create_bubble = null;
        //地圖格子座標
        _this.map_x = [];
        _this.map_y = [];
        _this.map_x_placed = [];
        _this.map_y_placed = [];
        _this.attacking = false;
        _this.begin_attack = true;
        return _this;
    }
    bubble_gen.prototype.onLoad = function () {
        //map
        for (var i = 0, ini_x = 140; i < 15; i++) {
            this.map_x.push(ini_x);
            ini_x -= 40;
            this.map_x_placed.push(false);
        }
        for (var j = 0, ini_y = -260; j < 13; j++) {
            this.map_y.push(ini_y);
            ini_y += 40;
            this.map_y_placed.push(false);
        }
    };
    bubble_gen.prototype.start = function () {
        this.scheduleOnce(this.attack1.bind(this), 5);
        this.scheduleOnce(this.attack2.bind(this), 8);
        this.scheduleOnce(this.attack3.bind(this), 9);
        this.scheduleOnce(function () {
            this.begin_attack = false;
        }, 10);
    };
    bubble_gen.prototype.update = function () {
        if (cc.find('Canvas/boss').getComponent('boss').boss_die)
            return;
        if (this.begin_attack)
            return;
        if (!this.attacking) {
            this.attacking = true;
            var mode = Math.floor(Math.random() * 4);
            if (mode == 0) {
                this.scheduleOnce(this.attack2.bind(this), 5);
                this.scheduleOnce(this.attack3.bind(this), 6);
            }
            else if (mode == 1 || mode == 2 || mode == 3) {
                this.scheduleOnce(this.random_attack.bind(this), 5);
            }
            this.scheduleOnce(function () {
                this.attacking = false;
            }, 8);
        }
    };
    bubble_gen.prototype.attack1 = function () {
        var bubble_x = [100, 60, 100, 140];
        var bubble_y = [-180, -220, -260, -220];
        for (var i = 0; i < 4; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(50, 170), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
        bubble_x = [100, 60, 100, 140];
        bubble_y = [140, 180, 220, 180];
        for (var i = 0; i < 4; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(50, -210), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
        bubble_x = [-380, -420, -380, -340];
        bubble_y = [140, 180, 220, 180];
        for (var i = 0; i < 4; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(-330, -210), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
        bubble_x = [-380, -420, -380, -340];
        bubble_y = [-260, -220, -180, -220];
        for (var i = 0; i < 4; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(-330, 170), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
    };
    bubble_gen.prototype.attack2 = function () {
        if (cc.find('Canvas/boss').getComponent('boss').boss_die)
            return;
        var bubble_x = [-20, 20, 60, 100];
        var bubble_y = [-140, -180, -220, -260];
        for (var i = 0; i < 4; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(50, -210), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
        bubble_x = [-260, -300, -340, -380];
        bubble_y = [100, 140, 180, 220];
        for (var i = 0; i < 4; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(-330, 170), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
    };
    bubble_gen.prototype.attack3 = function () {
        if (cc.find('Canvas/boss').getComponent('boss').boss_die)
            return;
        var bubble_x = [-260, -300, -340, -380, -420, -220, -180, -140, -100];
        var bubble_y = [-100, -60, -20, 20, 60, -140, -180, -220, -260];
        for (var i = 0; i < 9; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(50, -210), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
        bubble_x = [-60, -100, -140, -180, -20, 20, 60, 100, 140];
        bubble_y = [100, 140, 180, 220, 60, 20, -20, -60, -100];
        for (var i = 0; i < 9; i++) {
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(-330, 170), cc.v2(bubble_x[i], bubble_y[i])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
    };
    bubble_gen.prototype.random_attack = function () {
        if (cc.find('Canvas/boss').getComponent('boss').boss_die)
            return;
        for (var i = 0; i < 15; i++) {
            var random_x = Math.floor(Math.random() * 15);
            var random_y = Math.floor(Math.random() * 13);
            while ((this.map_x_placed[random_x] == true && this.map_y_placed[random_y] == true) || ((5 <= random_x && random_x <= 9) && (4 <= random_y && random_y <= 8))) {
                var random_x = Math.floor(Math.random() * 15);
                var random_y = Math.floor(Math.random() * 13);
            }
            this.map_x_placed[random_x] = true;
            this.map_y_placed[random_y] = true;
            var bubble = cc.instantiate(this.bubble);
            bubble.x = -140;
            bubble.y = -20;
            cc.find('Canvas/boss_bubble').addChild(bubble);
            var bezier = [cc.v2(-140, -20), cc.v2(-330, 170), cc.v2(this.map_x[random_x], this.map_y[random_y])];
            var action = cc.bezierTo(2, bezier);
            bubble.runAction(action);
        }
        for (var i = 0; i < 15; i++) {
            this.map_x_placed[i] = false;
        }
        for (var i = 0; i < 13; i++) {
            this.map_y_placed[i] = false;
        }
    };
    __decorate([
        property(cc.Prefab)
    ], bubble_gen.prototype, "bubble", void 0);
    bubble_gen = __decorate([
        ccclass
    ], bubble_gen);
    return bubble_gen;
}(cc.Component));
exports.default = bubble_gen;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=bubble_gen.js.map
        