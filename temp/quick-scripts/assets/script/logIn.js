(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/logIn.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '12457LBbkJOqKyXrwkaet+f', 'logIn', __filename);
// script/logIn.ts

Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameMgr_1 = require("./GameMgr");
var LogIn = /** @class */ (function (_super) {
    __extends(LogIn, _super);
    function LogIn() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.login = null;
        _this.signin = null;
        _this.password = null;
        _this.email = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    LogIn.prototype.onLoad = function () {
        this.login.on(cc.Node.EventType.MOUSE_DOWN, this.clickLogin, this);
        this.signin.on(cc.Node.EventType.MOUSE_DOWN, this.clickRegister, this);
    };
    LogIn.prototype.start = function () {
    };
    LogIn.prototype.changeVal = function (u_email) {
        var email = '';
        for (var i in u_email) {
            if (u_email[i] == '.' || u_email[i] == '#' || u_email[i] == '$' || u_email[i] == '[' || u_email[i] == ']')
                email += '-';
            else
                email += u_email[i];
        }
        return email;
    };
    LogIn.prototype.clickLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var user_email, user_pwd, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user_email = this.email.getComponent(cc.EditBox).string;
                        user_pwd = this.password.getComponent(cc.EditBox).string;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, firebase.auth().signInWithEmailAndPassword(user_email, user_pwd)];
                    case 2:
                        _a.sent();
                        cc.find('GameMgr').getComponent(GameMgr_1.default).email = this.changeVal(user_email);
                        firebase.database().ref(this.changeVal(user_email)).once('value', function (snapshot) {
                            cc.find('GameMgr').getComponent(GameMgr_1.default).money = snapshot.val().money;
                            cc.find('GameMgr').getComponent(GameMgr_1.default).pin = snapshot.val().pin;
                            cc.find('GameMgr').getComponent(GameMgr_1.default).buyflag = snapshot.val().buyflag;
                        });
                        cc.director.loadScene('lobby');
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        alert(error_1.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LogIn.prototype.clickRegister = function () {
        return __awaiter(this, void 0, void 0, function () {
            var user_email, user_pwd, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user_email = this.email.getComponent(cc.EditBox).string;
                        user_pwd = this.password.getComponent(cc.EditBox).string;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, firebase.auth().createUserWithEmailAndPassword(user_email, user_pwd)];
                    case 2:
                        _a.sent();
                        cc.find('GameMgr').getComponent(GameMgr_1.default).email = this.changeVal(user_email);
                        cc.find('GameMgr').getComponent(GameMgr_1.default).money = 19999;
                        cc.find('GameMgr').getComponent(GameMgr_1.default).pin = 0;
                        cc.find('GameMgr').getComponent(GameMgr_1.default).buyflag = 1;
                        firebase.database().ref(this.changeVal(user_email)).set({
                            money: 19999,
                            pin: 0,
                            buyflag: 1
                        });
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        alert(error_2.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        property(cc.Node)
    ], LogIn.prototype, "login", void 0);
    __decorate([
        property(cc.Node)
    ], LogIn.prototype, "signin", void 0);
    __decorate([
        property(cc.Node)
    ], LogIn.prototype, "password", void 0);
    __decorate([
        property(cc.Node)
    ], LogIn.prototype, "email", void 0);
    LogIn = __decorate([
        ccclass
    ], LogIn);
    return LogIn;
}(cc.Component));
exports.default = LogIn;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=logIn.js.map
        