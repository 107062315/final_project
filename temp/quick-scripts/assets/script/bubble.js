(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/bubble.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '5df7cWgu1lMSZTJJ7XhVXlP', 'bubble', __filename);
// script/bubble.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var bubble = /** @class */ (function (_super) {
    __extends(bubble, _super);
    function bubble() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.flow_center = null;
        _this.flow_down = null;
        _this.flow_down_end = null;
        _this.flow_up = null;
        _this.flow_up_end = null;
        _this.flow_left = null;
        _this.flow_left_end = null;
        _this.flow_right = null;
        _this.flow_right_end = null;
        _this.out_of_bubble = false;
        _this.other_out_of_bubble = false;
        _this.player = null;
        _this.other_player = null;
        //水柱遇到障礙物停止
        _this.stop = false;
        //music
        _this.sound_script = null;
        _this.sound = null;
        return _this;
    }
    bubble.prototype.onLoad = function () {
        //music
        this.sound_script = cc.find('audio').getComponent('audio');
        this.sound = this.sound_script.audioClips;
        this.player = cc.find('Canvas/player');
        if (this.node.parent.name == 'other_bubble')
            this.power = cc.find('Canvas/other_prefab').getComponent('Other').other_power;
        else
            this.power = cc.find('Canvas/player').getComponent('player').bubble_power;
        this.block = cc.find('Canvas/block').children;
        var action = cc.repeatForever(cc.sequence(cc.scaleBy(0.25, 1, 0.8), cc.scaleBy(0.25, 1, 1.25)));
        this.node.runAction(action);
    };
    bubble.prototype.start = function () {
        this.other_player = cc.find('Canvas/other_prefab');
        this.scheduleOnce(this.explode, 1.5);
    };
    bubble.prototype.update = function () {
        if (this.player.x < this.node.x - 30 || this.player.x > this.node.x + 30 || this.player.y < this.node.y - 30 || this.player.y > this.node.y + 30)
            this.out_of_bubble = true;
        if (this.other_player.x < this.node.x - 30 || this.other_player.x > this.node.x + 30 || this.other_player.y < this.node.y - 30 || this.other_player.y > this.node.y + 30)
            this.other_out_of_bubble = true;
    };
    bubble.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'player') {
            if (this.out_of_bubble)
                contact.disabled = false;
            else
                contact.disabled = true;
        }
        if (other.node.name == 'other_prefab') {
            if (this.other_out_of_bubble)
                contact.disabled = false;
            else
                contact.disabled = true;
        }
        if (other.node.name == 'flow_left_end' || other.node.name == 'flow_right_end' || other.node.name == 'flow_up_end' || other.node.name == 'flow_down_end'
            || other.node.name == 'flow_left' || other.node.name == 'flow_right' || other.node.name == 'flow_up' || other.node.name == 'flow_down') {
            this.unschedule(this.explode);
            this.explode();
        }
    };
    bubble.prototype.explode = function () {
        cc.audioEngine.playEffect(this.sound[5], false);
        var flow_center = cc.instantiate(this.flow_center);
        flow_center.x = this.node.x;
        flow_center.y = this.node.y;
        cc.find('Canvas/bubble_explode').addChild(flow_center);
        //左水柱
        for (var i = 1; i <= this.power; i++) {
            if (i == this.power)
                var flow_left = cc.instantiate(this.flow_left_end);
            else
                var flow_left = cc.instantiate(this.flow_left);
            flow_left.x = this.node.x - i * 40;
            flow_left.y = this.node.y;
            if (flow_left.x >= -430)
                cc.find('Canvas/bubble_explode').addChild(flow_left);
            //水柱不能超出障礙物
            for (var j = 0; j < this.block.length; j++) {
                if (this.block[j].x == flow_left.x && this.block[j].y == flow_left.y) {
                    this.stop = true;
                    break;
                }
            }
            if (this.stop) {
                this.stop = false;
                break;
            }
        }
        //右水柱
        for (var i = 1; i <= this.power; i++) {
            if (i == this.power)
                var flow_right = cc.instantiate(this.flow_right_end);
            else
                var flow_right = cc.instantiate(this.flow_right);
            flow_right.x = this.node.x + i * 40;
            flow_right.y = this.node.y;
            if (flow_right.x <= 160)
                cc.find('Canvas/bubble_explode').addChild(flow_right);
            //水柱不能超出障礙物
            for (var j = 0; j < this.block.length; j++) {
                if (this.block[j].x == flow_right.x && this.block[j].y == flow_right.y) {
                    this.stop = true;
                    break;
                }
            }
            if (this.stop) {
                this.stop = false;
                break;
            }
        }
        //上水柱
        for (var i = 1; i <= this.power; i++) {
            if (i == this.power)
                var flow_up = cc.instantiate(this.flow_up_end);
            else
                var flow_up = cc.instantiate(this.flow_up);
            flow_up.x = this.node.x;
            flow_up.y = this.node.y + i * 40;
            if (flow_up.y <= 220)
                cc.find('Canvas/bubble_explode').addChild(flow_up);
            //水柱不能超出障礙物
            for (var j = 0; j < this.block.length; j++) {
                if (this.block[j].x == flow_up.x && this.block[j].y == flow_up.y) {
                    this.stop = true;
                    break;
                }
            }
            if (this.stop) {
                this.stop = false;
                break;
            }
        }
        //下水柱
        for (var i = 1; i <= this.power; i++) {
            if (i == this.power)
                var flow_down = cc.instantiate(this.flow_down_end);
            else
                var flow_down = cc.instantiate(this.flow_down);
            flow_down.x = this.node.x;
            flow_down.y = this.node.y - i * 40;
            if (flow_down.y >= -260)
                cc.find('Canvas/bubble_explode').addChild(flow_down);
            //水柱不能超出障礙物
            for (var j = 0; j < this.block.length; j++) {
                if (this.block[j].x == flow_down.x && this.block[j].y == flow_down.y) {
                    this.stop = true;
                    break;
                }
            }
            if (this.stop) {
                this.stop = false;
                break;
            }
        }
        this.node.destroy();
    };
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_center", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_down", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_down_end", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_up", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_up_end", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_left", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_left_end", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_right", void 0);
    __decorate([
        property(cc.Prefab)
    ], bubble.prototype, "flow_right_end", void 0);
    bubble = __decorate([
        ccclass
    ], bubble);
    return bubble;
}(cc.Component));
exports.default = bubble;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=bubble.js.map
        