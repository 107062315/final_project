(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Other.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '62294wik35Jkqqdnp5I1JvZ', 'Other', __filename);
// script/Other.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Other = /** @class */ (function (_super) {
    __extends(Other, _super);
    function Other() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // animation
        _this.anim = null;
        _this.animateState = null;
        //other bubble power
        _this.other_power = 1;
        _this.stuck = false;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Other.prototype.onLoad = function () {
        this.anim = this.node.getComponent(cc.Animation);
        this.animateState = this.anim.play('player_idle_down');
    };
    Other.prototype.start = function () {
    };
    // update (dt) {}
    Other.prototype.playAnimation = function (newAnimateState) {
        if (this.animateState.name != newAnimateState) {
            this.animateState = this.anim.play(newAnimateState);
        }
    };
    Other.prototype.onPreSolve = function (contact, self, other) {
        if (other.node.parent.name == 'block') {
            if (this.stuck)
                contact.disabled = true;
            else
                contact.disabled = false;
        }
    };
    Other.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'add_power') {
            this.other_power++;
        }
    };
    Other = __decorate([
        ccclass
    ], Other);
    return Other;
}(cc.Component));
exports.default = Other;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Other.js.map
        