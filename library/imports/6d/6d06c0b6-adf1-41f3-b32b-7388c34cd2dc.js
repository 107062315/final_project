"use strict";
cc._RF.push(module, '6d06cC2rfFB87Mrc4jDTNLc', 'boss');
// script/boss1/boss.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var boss = /** @class */ (function (_super) {
    __extends(boss, _super);
    function boss() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //music
        _this.sound_script = null;
        _this.sound = null;
        _this.anim = null;
        _this.animstate = null;
        _this.HP = 3;
        _this.boss_die = false;
        _this.been_attacked = false;
        return _this;
    }
    boss.prototype.onLoad = function () {
        //music
        this.sound_script = cc.find('audio').getComponent('audio');
        this.sound = this.sound_script.audioClips;
        this.anim = this.node.getComponent(cc.Animation);
        this.animstate = this.anim.play();
        this.HP_width = cc.find('Canvas/boss_HPframe/boss_HP').width;
    };
    boss.prototype.update = function () {
        if (this.boss_die)
            return;
        if (this.HP == 0) {
            cc.audioEngine.playEffect(this.sound[3], false);
            this.boss_die = true;
            this.anim.play('boss_die');
            this.scheduleOnce(function () {
                cc.audioEngine.playEffect(this.sound[4], false);
            }, 2);
            this.scheduleOnce(this.win, 4);
            this.scheduleOnce(function () {
                cc.director.loadScene('lobby');
            }, 8);
        }
        if (!this.animstate.isPlaying && this.HP != 0) {
            this.animstate = this.anim.play();
        }
    };
    boss.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'flow_left_end' || other.node.name == 'flow_right_end' || other.node.name == 'flow_up_end' || other.node.name == 'flow_down_end'
            || other.node.name == 'flow_left' || other.node.name == 'flow_right' || other.node.name == 'flow_up' || other.node.name == 'flow_down') {
            if (this.been_attacked)
                return;
            this.animstate = this.anim.play('boss_hurt');
            if (this.HP != 0) {
                this.HP -= 1;
                this.been_attacked = true;
                cc.find('Canvas/boss_HPframe/boss_HP').width -= this.HP_width * 1 / 3;
                cc.find('Canvas/boss_HPframe/boss_HP').x -= (this.HP_width * (1 / 3) * (1 / 2));
            }
        }
    };
    boss.prototype.onEndContact = function (contact, self, other) {
        this.been_attacked = false;
    };
    boss.prototype.win = function () {
        cc.audioEngine.playMusic(this.sound[6], false);
        var action = cc.moveBy(3, 0, -300);
        cc.find('Canvas/win').runAction(action);
        this.node.opacity = 0;
    };
    boss = __decorate([
        ccclass
    ], boss);
    return boss;
}(cc.Component));
exports.default = boss;

cc._RF.pop();