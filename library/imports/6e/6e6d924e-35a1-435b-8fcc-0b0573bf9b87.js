"use strict";
cc._RF.push(module, '6e6d9JONaFDW4/MCwVzv5uH', 'move_block');
// script/move_block.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var move_block = /** @class */ (function (_super) {
    __extends(move_block, _super);
    function move_block() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.move_down_cancel = false;
        _this.move_up_cancel = false;
        _this.move_left_cancel = false;
        _this.move_right_cancel = false;
        _this.other_move_down_cancel = false;
        _this.other_move_up_cancel = false;
        _this.other_move_left_cancel = false;
        _this.other_move_right_cancel = false;
        _this.other_anim = null;
        return _this;
    }
    move_block.prototype.onLoad = function () {
        this.player_anim = cc.find('Canvas/player').getComponent(cc.Animation);
    };
    move_block.prototype.update = function (dt) {
        if (!cc.find('Canvas/player') || !cc.find('Canvas/other_prefab')) {
            this.move_up_cancel = true;
            this.move_down_cancel = true;
            this.move_left_cancel = true;
            this.move_right_cancel = true;
            this.other_move_up_cancel = true;
            this.other_move_down_cancel = true;
            this.other_move_left_cancel = true;
            this.other_move_right_cancel = true;
            return;
        }
        var player_up = this.player_anim.getAnimationState('player_move_up');
        var player_down = this.player_anim.getAnimationState('player_move_down');
        var player_left = this.player_anim.getAnimationState('player_move_left');
        var player_right = this.player_anim.getAnimationState('player_move_right');
        var pirate_player_up = this.player_anim.getAnimationState('pirate_move_up');
        var pirate_player_down = this.player_anim.getAnimationState('pirate_move_down');
        var pirate_player_left = this.player_anim.getAnimationState('pirate_move_left');
        var pirate_player_right = this.player_anim.getAnimationState('pirate_move_right');
        if (cc.find('Canvas/other_prefab'))
            this.other_anim = cc.find('Canvas/other_prefab').getComponent(cc.Animation);
        if (this.other_anim) {
            var other_up = this.other_anim.getAnimationState('player_move_up');
            var other_down = this.other_anim.getAnimationState('player_move_down');
            var other_left = this.other_anim.getAnimationState('player_move_left');
            var other_right = this.other_anim.getAnimationState('player_move_right');
            var pirate_other_up = this.other_anim.getAnimationState('pirate_move_up');
            var pirate_other_down = this.other_anim.getAnimationState('pirate_move_down');
            var pirate_other_left = this.other_anim.getAnimationState('pirate_move_left');
            var pirate_other_right = this.other_anim.getAnimationState('pirate_move_right');
            if (!player_up.isPlaying && !pirate_player_up.isPlaying) {
                this.move_up_cancel = true;
            }
            if (!player_down.isPlaying && !pirate_player_down.isPlaying) {
                this.move_down_cancel = true;
            }
            if (!player_left.isPlaying && !pirate_player_left.isPlaying) {
                this.move_left_cancel = true;
            }
            if (!player_right.isPlaying && !pirate_player_right.isPlaying) {
                this.move_right_cancel = true;
            }
            if (!other_up.isPlaying && !pirate_other_up.isPlaying) {
                this.other_move_up_cancel = true;
            }
            if (!other_down.isPlaying && !pirate_other_down.isPlaying) {
                this.other_move_down_cancel = true;
            }
            if (!other_left.isPlaying && !pirate_other_left.isPlaying) {
                this.other_move_left_cancel = true;
            }
            if (!other_right.isPlaying && !pirate_other_right.isPlaying) {
                this.other_move_right_cancel = true;
            }
        }
    };
    move_block.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'player') {
            this.schedule(this.move_up, 0.6);
            this.schedule(this.move_down, 0.6);
            this.schedule(this.move_left, 0.6);
            this.schedule(this.move_right, 0.6);
        }
        if (other.node.name == 'other_prefab') {
            this.schedule(this.other_move_up, 0.6);
            this.schedule(this.other_move_down, 0.6);
            this.schedule(this.other_move_left, 0.6);
            this.schedule(this.other_move_right, 0.6);
        }
    };
    move_block.prototype.onEndContact = function (contact, self, other) {
        if (other.node.name == 'player') {
            this.unschedule(this.move_up);
            this.unschedule(this.move_down);
            this.unschedule(this.move_left);
            this.unschedule(this.move_right);
            this.move_up_cancel = false;
            this.move_down_cancel = false;
            this.move_left_cancel = false;
            this.move_right_cancel = false;
        }
        if (other.node.name == 'other_prefab') {
            this.unschedule(this.other_move_up);
            this.unschedule(this.other_move_down);
            this.unschedule(this.other_move_left);
            this.unschedule(this.other_move_right);
            this.other_move_up_cancel = false;
            this.other_move_down_cancel = false;
            this.other_move_left_cancel = false;
            this.other_move_right_cancel = false;
        }
    };
    move_block.prototype.move_up = function () {
        if (this.move_up_cancel) {
            this.move_up_cancel = false;
            return;
        }
        if (this.node.y + 40 == 260)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x && block[i].y == this.node.y + 40 && block[i].name != 'grass')
                return;
        }
        var ini_y = this.node.y;
        var finished = cc.callFunc(function () {
            this.node.y = ini_y + 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, 0, 40), finished);
        this.node.runAction(action);
        this.move_up_cancel = true;
    };
    move_block.prototype.other_move_up = function () {
        if (this.other_move_up_cancel) {
            this.other_move_up_cancel = false;
            return;
        }
        if (this.node.y + 40 == 260)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x && block[i].y == this.node.y + 40 && block[i].name != 'grass')
                return;
        }
        var ini_y = this.node.y;
        var finished = cc.callFunc(function () {
            this.node.y = ini_y + 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, 0, 40), finished);
        this.node.runAction(action);
        this.other_move_up_cancel = true;
    };
    move_block.prototype.move_down = function () {
        if (this.move_down_cancel) {
            this.move_down_cancel = false;
            return;
        }
        if (this.node.y - 40 == -300)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x && block[i].y == this.node.y - 40 && block[i].name != 'grass')
                return;
        }
        var ini_y = this.node.y;
        var finished = cc.callFunc(function () {
            this.node.y = ini_y - 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, 0, -40), finished);
        this.node.runAction(action);
        this.move_down_cancel = true;
    };
    move_block.prototype.other_move_down = function () {
        if (this.other_move_down_cancel) {
            this.other_move_down_cancel = false;
            return;
        }
        if (this.node.y - 40 == -300)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x && block[i].y == this.node.y - 40 && block[i].name != 'grass')
                return;
        }
        var ini_y = this.node.y;
        var finished = cc.callFunc(function () {
            this.node.y = ini_y - 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, 0, -40), finished);
        this.node.runAction(action);
        this.other_move_down_cancel = true;
    };
    move_block.prototype.move_left = function () {
        if (this.move_left_cancel) {
            this.move_left_cancel = false;
            return;
        }
        if (this.node.x - 40 == -460)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x - 40 && block[i].y == this.node.y && block[i].name != 'grass')
                return;
        }
        var ini_x = this.node.x;
        var finished = cc.callFunc(function () {
            this.node.x = ini_x - 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, -40, 0), finished);
        this.node.runAction(action);
        console.log("this_move_left");
        this.move_left_cancel = true;
    };
    move_block.prototype.other_move_left = function () {
        if (this.other_move_left_cancel) {
            this.other_move_left_cancel = false;
            return;
        }
        if (this.node.x - 40 == -460)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x - 40 && block[i].y == this.node.y && block[i].name != 'grass')
                return;
        }
        var ini_x = this.node.x;
        var finished = cc.callFunc(function () {
            this.node.x = ini_x - 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, -40, 0), finished);
        this.node.runAction(action);
        console.log("other_move_left");
        this.other_move_left_cancel = true;
    };
    move_block.prototype.move_right = function () {
        if (this.move_right_cancel) {
            this.move_right_cancel = false;
            return;
        }
        if (this.node.x + 40 == 180)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x + 40 && block[i].y == this.node.y && block[i].name != 'grass')
                return;
        }
        var ini_x = this.node.x;
        var finished = cc.callFunc(function () {
            this.node.x = ini_x + 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, 40, 0), finished);
        this.node.runAction(action);
        this.move_right_cancel = true;
    };
    move_block.prototype.other_move_right = function () {
        if (this.other_move_right_cancel) {
            this.other_move_right_cancel = false;
            return;
        }
        if (this.node.x + 40 == 180)
            return;
        var block = cc.find('Canvas/block').children;
        for (var i = 0; i < block.length; i++) {
            if (block[i].x == this.node.x + 40 && block[i].y == this.node.y && block[i].name != 'grass')
                return;
        }
        var ini_x = this.node.x;
        var finished = cc.callFunc(function () {
            this.node.x = ini_x + 40;
        }, this);
        var action = cc.sequence(cc.moveBy(0.6, 40, 0), finished);
        this.node.runAction(action);
        this.other_move_right_cancel = true;
    };
    move_block = __decorate([
        ccclass
    ], move_block);
    return move_block;
}(cc.Component));
exports.default = move_block;

cc._RF.pop();