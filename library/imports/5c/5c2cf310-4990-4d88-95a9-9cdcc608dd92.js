"use strict";
cc._RF.push(module, '5c2cfMQSZBNiJWpnNzGCN2S', 'bgm_play');
// script/audio/bgm_play.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var bgm_play = /** @class */ (function (_super) {
    __extends(bgm_play, _super);
    function bgm_play() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sound_script = null;
        _this.sound = null;
        return _this;
    }
    bgm_play.prototype.onLoad = function () {
        this.sound_script = cc.find('audio').getComponent('audio');
        this.sound = this.sound_script.audioClips;
    };
    bgm_play.prototype.start = function () {
        if (this.sound[1]) {
            cc.audioEngine.playMusic(this.sound[0], false);
            this.scheduleOnce(function () {
                cc.audioEngine.playMusic(this.sound[1], true);
            }, 1);
        }
        else {
            cc.audioEngine.playMusic(this.sound[0], true);
        }
    };
    bgm_play = __decorate([
        ccclass
    ], bgm_play);
    return bgm_play;
}(cc.Component));
exports.default = bgm_play;

cc._RF.pop();