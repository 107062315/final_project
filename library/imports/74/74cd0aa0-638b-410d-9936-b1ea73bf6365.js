"use strict";
cc._RF.push(module, '74cd0qgY4tBDZk2sepzv2Nl', 'roomowner');
// script/roomowner.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var roomowner = /** @class */ (function (_super) {
    __extends(roomowner, _super);
    function roomowner() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.roomown = false;
        return _this;
    }
    roomowner.prototype.onLoad = function () {
        cc.game.addPersistRootNode(this.node);
    };
    roomowner = __decorate([
        ccclass
    ], roomowner);
    return roomowner;
}(cc.Component));
exports.default = roomowner;

cc._RF.pop();