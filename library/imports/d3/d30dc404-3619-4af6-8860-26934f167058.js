"use strict";
cc._RF.push(module, 'd30dcQENhlK9ohgJpNPFnBY', 'flow_end');
// script/flow_end.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var flow_end = /** @class */ (function (_super) {
    __extends(flow_end, _super);
    function flow_end() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    flow_end.prototype.onLoad = function () {
        this.anim = this.node.getComponent(cc.Animation);
    };
    flow_end.prototype.start = function () {
        this.anim.play();
        this.scheduleOnce(function () {
            this.node.destroy();
        }, 0.6);
    };
    flow_end = __decorate([
        ccclass
    ], flow_end);
    return flow_end;
}(cc.Component));
exports.default = flow_end;

cc._RF.pop();