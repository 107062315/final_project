"use strict";
cc._RF.push(module, '26ea5prBwlAmKOQ2PgpLLW0', 'Lobby');
// script/Lobby.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameMgr_1 = require("./GameMgr");
var Lobby = /** @class */ (function (_super) {
    __extends(Lobby, _super);
    function Lobby() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.createRoom_button = null;
        _this.joinRoom_button = null;
        _this.shop_button = null;
        _this.roomID = null;
        _this.roomContainer = null;
        _this.room_prefab = null;
        _this.GameMgr = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Lobby.prototype.onLoad = function () {
        this.createRoom_button.on(cc.Node.EventType.MOUSE_DOWN, this.createRoom, this);
        this.joinRoom_button.on(cc.Node.EventType.MOUSE_DOWN, this.joinRoom, this);
        this.shop_button.on(cc.Node.EventType.MOUSE_DOWN, this.toShop, this);
        this.GameMgr = cc.find("GameMgr").getComponent(GameMgr_1.default);
        this.GameMgr.scene = "Lobby";
        this.GameMgr.isOwner = false;
        this.GameMgr.inRoom = false;
        this.GameMgr.other_id = -1;
        /*firebase.database().ref("rooms").on("child_added", (snapshot) => {
            var newRoom = cc.instantiate(this.room_prefab);
            newRoom.getChildByName("roomName").getComponent(cc.Label).string = snapshot.val().id;
            this.roomContainer.addChild(newRoom);
        });*/
    };
    Lobby.prototype.start = function () {
    };
    //update (dt) {}
    Lobby.prototype.createRoom = function () {
        //var newRoom = cc.instantiate(this.room_prefab);
        //this.roomContainer.addChild(newRoom);
        /*firebase.database().ref("rooms").push({
            id: this.GameMgr.cliend_id
        });*/
        this.GameMgr.inRoom = this.GameMgr.isOwner = true;
        this.GameMgr.isSingle = false;
        this.GameMgr.mode = "PVP";
        cc.director.loadScene('waitingroom');
    };
    Lobby.prototype.toShop = function () {
        cc.director.loadScene('Shop');
    };
    Lobby.prototype.joinRoom = function () {
        var roomNumber = parseInt(this.roomID.getComponent(cc.EditBox).string, 10);
        if (!isNaN(roomNumber)) {
            this.GameMgr.ws.send(JSON.stringify({
                type: "JOIN_req",
                other_id: roomNumber,
                sponsor: this.GameMgr.cliend_id
            }));
        }
    };
    Lobby.prototype.boss1 = function () {
        cc.director.loadScene('boss1');
    };
    __decorate([
        property(cc.Node)
    ], Lobby.prototype, "createRoom_button", void 0);
    __decorate([
        property(cc.Node)
    ], Lobby.prototype, "joinRoom_button", void 0);
    __decorate([
        property(cc.Node)
    ], Lobby.prototype, "shop_button", void 0);
    __decorate([
        property(cc.Node)
    ], Lobby.prototype, "roomID", void 0);
    __decorate([
        property(cc.Node)
    ], Lobby.prototype, "roomContainer", void 0);
    __decorate([
        property(cc.Prefab)
    ], Lobby.prototype, "room_prefab", void 0);
    Lobby = __decorate([
        ccclass
    ], Lobby);
    return Lobby;
}(cc.Component));
exports.default = Lobby;

cc._RF.pop();