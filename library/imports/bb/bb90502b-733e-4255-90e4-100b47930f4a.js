"use strict";
cc._RF.push(module, 'bb905Arcz5CVZDkEAtHkw9K', 'audio');
// script/audio/audio.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var audio = /** @class */ (function (_super) {
    __extends(audio, _super);
    function audio() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.audioClips = [];
        return _this;
    }
    __decorate([
        property({ type: cc.AudioClip })
    ], audio.prototype, "audioClips", void 0);
    audio = __decorate([
        ccclass
    ], audio);
    return audio;
}(cc.Component));
exports.default = audio;

cc._RF.pop();