"use strict";
cc._RF.push(module, 'fc727wQjnREvYv/6VibLR7L', 'waiting_room');
// script/waiting_room.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("./GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Waitingroom = /** @class */ (function (_super) {
    __extends(Waitingroom, _super);
    function Waitingroom() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.lobby_button = null;
        _this.PVP_boss_button = null; //boss or PVP
        _this.mode_button = null; //one or two players
        _this.GO_button = null;
        _this.boss_scene = null;
        _this.boss_label = null;
        _this.PVP_label = null;
        _this.mode_btn_img = null;
        _this.one_label = null;
        _this.two_label = null;
        _this.waitfriend_off = null;
        _this.roomID_Label = null;
        _this.Bossflag = false;
        _this.GameMgr = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Waitingroom.prototype.onLoad = function () {
        this.GameMgr = cc.find("GameMgr").getComponent(GameMgr_1.default);
        this.lobby_button.on(cc.Node.EventType.MOUSE_DOWN, this.toLobby, this);
        if (this.GameMgr.isOwner) {
            this.PVP_boss_button.on(cc.Node.EventType.MOUSE_DOWN, this.clickPVPorboss, this);
            this.mode_button.on(cc.Node.EventType.MOUSE_DOWN, this.clickmode, this);
            this.GO_button.on(cc.Node.EventType.MOUSE_DOWN, this.gameStart, this);
            this.roomID_Label.string = this.GameMgr.cliend_id.toString(10);
        }
        else {
            this.PVP_boss_button.active = false;
            this.GO_button.active = false;
            this.roomID_Label.node.active = false;
            cc.find("P2").active = true;
        }
        if (this.GameMgr.mode != "BOSS")
            cc.find("Canvas/boss_bg").active = false;
        cc.find("Canvas/PVP_boss_btn_img/boss Label").active = false;
        cc.find("Canvas/mode_btn_img").active = false;
        cc.find("Canvas/mode_btn_img/two Label").active = false;
        cc.find("Canvas/waitfriend_off").active = false;
    };
    Waitingroom.prototype.start = function () {
    };
    Waitingroom.prototype.toLobby = function () {
        cc.director.loadScene('lobby');
    };
    Waitingroom.prototype.clickPVPorboss = function () {
        if (this.GameMgr.isOwner == false)
            return;
        this.Bossflag = !this.Bossflag;
        if (this.GameMgr.other_id != -1) {
            cc.find("Canvas/mode_btn_img/one Label").active = false;
            cc.find("Canvas/mode_btn_img/two Label").active = true;
            cc.find("Canvas/waitfriend_off").active = false;
        }
        if (this.Bossflag) {
            this.GameMgr.mode = "BOSS";
            cc.find("Canvas/boss_bg").active = true;
            cc.find("Canvas/PVP_boss_btn_img/boss Label").active = true;
            cc.find("Canvas/PVP_boss_btn_img/PVP Label").active = false;
            cc.find("Canvas/mode_btn_img").active = true;
            if (!this.GameMgr.isSingle) {
                cc.find("Canvas/waitfriend_off").active = false;
                this.GameMgr.ws.send(JSON.stringify({
                    type: "GAME_MODE",
                    mode: "BOSS",
                    other_id: this.GameMgr.other_id
                }));
            }
            else {
                cc.find("Canvas/waitfriend_off").active = true;
            }
        }
        else {
            this.GameMgr.mode = "PVP";
            cc.find("Canvas/boss_bg").active = false;
            cc.find("Canvas/PVP_boss_btn_img/boss Label").active = false;
            cc.find("Canvas/PVP_boss_btn_img/PVP Label").active = true;
            cc.find("Canvas/mode_btn_img").active = false;
            cc.find("Canvas/waitfriend_off").active = false;
            this.GameMgr.isSingle = false;
            this.GameMgr.ws.send(JSON.stringify({
                type: "GAME_MODE",
                mode: "PVP",
                other_id: this.GameMgr.other_id
            }));
        }
    };
    Waitingroom.prototype.clickmode = function () {
        if (this.GameMgr.isOwner == false)
            return;
        if (this.GameMgr.other_id != -1) {
            return;
        }
        this.GameMgr.isSingle = !this.GameMgr.isSingle;
        if (!this.GameMgr.isSingle) {
            cc.find("Canvas/mode_btn_img/one Label").active = false;
            cc.find("Canvas/mode_btn_img/two Label").active = true;
            cc.find("Canvas/waitfriend_off").active = false;
        }
        else {
            cc.find("Canvas/mode_btn_img/one Label").active = true;
            cc.find("Canvas/mode_btn_img/two Label").active = false;
            cc.find("Canvas/waitfriend_off").active = true;
        }
    };
    Waitingroom.prototype.update = function (dt) {
        if (!this.GameMgr.isSingle) {
            cc.find("Canvas/mode_btn_img/one Label").active = false;
            cc.find("Canvas/mode_btn_img/two Label").active = true;
            cc.find("Canvas/waitfriend_off").active = false;
        }
        else {
            cc.find("Canvas/mode_btn_img/one Label").active = true;
            cc.find("Canvas/mode_btn_img/two Label").active = false;
            cc.find("Canvas/waitfriend_off").active = true;
        }
    };
    Waitingroom.prototype.gameStart = function () {
        if (this.GameMgr.isSingle && this.Bossflag) {
            cc.director.loadScene("boss1");
        }
        else if (!this.GameMgr.isSingle && this.Bossflag) {
            if (this.GameMgr.other_id != -1) {
                this.GameMgr.ws.send(JSON.stringify({
                    type: "GAME_START",
                    other_id: this.GameMgr.other_id
                }));
            }
        }
        else if (!this.Bossflag) {
            if (this.GameMgr.other_id != -1) {
                this.GameMgr.ws.send(JSON.stringify({
                    type: "GAME_START",
                    other_id: this.GameMgr.other_id
                }));
                cc.director.loadScene('game');
            }
        }
    };
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "lobby_button", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "PVP_boss_button", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "mode_button", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "GO_button", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "boss_scene", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "boss_label", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "PVP_label", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "mode_btn_img", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "one_label", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "two_label", void 0);
    __decorate([
        property(cc.Node)
    ], Waitingroom.prototype, "waitfriend_off", void 0);
    __decorate([
        property(cc.Label)
    ], Waitingroom.prototype, "roomID_Label", void 0);
    Waitingroom = __decorate([
        ccclass
    ], Waitingroom);
    return Waitingroom;
}(cc.Component));
exports.default = Waitingroom;

cc._RF.pop();