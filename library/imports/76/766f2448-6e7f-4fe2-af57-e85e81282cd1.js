"use strict";
cc._RF.push(module, '766f2RIbn9P4q9X6F6BKCzR', 'block');
// script/block.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameMgr_1 = require("./GameMgr");
var block = /** @class */ (function (_super) {
    __extends(block, _super);
    function block() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.already_destroy = false;
        //道具
        _this.add_num = null;
        _this.add_power = null;
        _this.add_speed = null;
        _this.mgr = null;
        _this.itemGen = null;
        _this.have_item = -1;
        _this.which_item = -1;
        return _this;
    }
    block.prototype.onLoad = function () {
        this.mgr = cc.find("GameMgr").getComponent(GameMgr_1.default);
        this.itemGen = cc.find('Canvas/itemGen');
        this.anim = this.node.getComponent(cc.Animation);
    };
    block.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'flow_left_end' || other.node.name == 'flow_right_end' || other.node.name == 'flow_up_end' || other.node.name == 'flow_down_end'
            || other.node.name == 'flow_left' || other.node.name == 'flow_right' || other.node.name == 'flow_up' || other.node.name == 'flow_down') {
            if (this.node.name == 'move_block')
                this.scheduleOnce(this.block_destroy, 0.25);
            else
                this.scheduleOnce(this.block_destroy, 0.65);
            this.animstate = this.anim.play();
        }
    };
    block.prototype.block_destroy = function () {
        if (this.have_item == 0) {
            var item;
            if (this.which_item == 0) {
                item = cc.instantiate(this.add_num);
            }
            else if (this.which_item == 1) {
                item = cc.instantiate(this.add_power);
            }
            else if (this.which_item == 2) {
                item = cc.instantiate(this.add_speed);
            }
            item.x = this.node.x;
            item.y = this.node.y;
            item.parent = cc.find('Canvas/item');
        }
        this.node.destroy();
    };
    block.prototype.sendDataToServer = function (type) {
        var additionalData = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additionalData[_i - 1] = arguments[_i];
        }
        switch (type) {
            case "ITEM":
                this.mgr.ws.send(JSON.stringify({
                    type: "ITEM",
                    other_id: this.mgr.other_id,
                    item_gen: additionalData[0],
                    which_item: additionalData[1],
                }));
                break;
            default:
                break;
        }
    };
    __decorate([
        property(cc.Prefab)
    ], block.prototype, "add_num", void 0);
    __decorate([
        property(cc.Prefab)
    ], block.prototype, "add_power", void 0);
    __decorate([
        property(cc.Prefab)
    ], block.prototype, "add_speed", void 0);
    block = __decorate([
        ccclass
    ], block);
    return block;
}(cc.Component));
exports.default = block;

cc._RF.pop();