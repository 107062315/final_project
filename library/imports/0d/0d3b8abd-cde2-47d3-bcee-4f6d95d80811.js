"use strict";
cc._RF.push(module, '0d3b8q9zeJH07zuT22V2AgR', 'change_scene');
// script/change_scene.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var change_scene = /** @class */ (function (_super) {
    __extends(change_scene, _super);
    function change_scene() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.die = false;
        return _this;
    }
    change_scene.prototype.update = function () {
        if (this.die == true) {
            this.scheduleOnce(function () {
                cc.director.loadScene('lobby');
            }, 4);
        }
    };
    change_scene = __decorate([
        ccclass
    ], change_scene);
    return change_scene;
}(cc.Component));
exports.default = change_scene;

cc._RF.pop();