"use strict";
cc._RF.push(module, '07382NWdJ1E9KEFFkaMcPJR', 'player');
// script/player.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameMgr_1 = require("./GameMgr");
var player = /** @class */ (function (_super) {
    __extends(player, _super);
    function player() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pin = -1;
        //music
        _this.sound_script = null;
        _this.sound = null;
        //被水球包起來不能動
        _this.stuck = false;
        //水球
        _this.bubble = null;
        //被水球包起來
        _this.player_in_bubble = null;
        //角色已經在水球裡
        _this.already_in_bubble = false;
        //目前水球威力
        _this.bubble_power = 1;
        //目前持有水球數量
        _this.bubble_num = 1;
        //地圖格子座標
        _this.map_x = [];
        _this.map_y = [];
        //上下左右按鍵
        _this.key_up = false;
        _this.key_down = false;
        _this.key_left = false;
        _this.key_right = false;
        //空白鍵放水球
        _this.key_space = false;
        //角色面對方向
        _this.face_up = false;
        _this.face_down = false;
        _this.face_left = false;
        _this.face_right = false;
        //角色移動速度的增加
        _this.speedX_add = 0;
        _this.speedY_add = 0;
        _this.animateState = null;
        _this.mgr = null;
        _this.has_reborn = true;
        return _this;
    }
    player.prototype.onLoad = function () {
        //animation
        this.anim = this.node.getComponent(cc.Animation);
        if (cc.find("GameMgr").getComponent('GameMgr').buyflag == 0)
            this.animateState = this.anim.play('pirate_idle_down');
        else
            this.animateState = this.anim.play('player_idle_down');
        //針
        var email = cc.find("GameMgr").getComponent('GameMgr').email;
        firebase.database().ref(email).once('value', function (snapshot) {
            cc.find('Canvas/player').getComponent('player').pin = snapshot.val().pin;
        });
        //music
        this.sound_script = cc.find('audio').getComponent('audio');
        this.sound = this.sound_script.audioClips;
        if (cc.find('GameMgr').getComponent('GameMgr').isOwner)
            this.node.setPosition(140, -260);
        else
            this.node.setPosition(-420, 220);
        this.mgr = cc.find("GameMgr").getComponent(GameMgr_1.default);
        this.mgr.player_node = this.node;
        this.mgr.player = this;
        this.mgr.scene = "game";
        //physics
        cc.director.getPhysicsManager().enabled = true;
        //keyboard
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        //map
        for (var i = 0, ini_x = 140; i < 15; i++) {
            this.map_x.push(ini_x);
            ini_x -= 40;
        }
        for (var j = 0, ini_y = -260; j < 13; j++) {
            this.map_y.push(ini_y);
            ini_y += 40;
        }
    };
    player.prototype.update = function (dt) {
        if (!this.node)
            return;
        //console.log(this.node.y);
        if (this.node.parent.getComponent('player_in_bubble')) {
            if (this.node.parent.getComponent('player_in_bubble').other_player_contact) {
                this.sendDataToServer("PLAYER_DIE");
                this.node.opacity = 0;
                this.unschedule(this.lose);
                this.unschedule(this.explode);
                this.scheduleOnce(this.lose, 1);
                this.scheduleOnce(this.explode, 0.5);
                this.node.parent.getComponent('player_in_bubble').other_player_contact = false;
            }
        }
        this.player_move(dt);
        this.player_face(dt);
        this.place_bubble(dt);
        //傳資料給server
        this.sendDataToServer("PLAYER_DATA");
    };
    player.prototype.sendDataToServer = function (type) {
        var additionalData = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additionalData[_i - 1] = arguments[_i];
        }
        switch (type) {
            case "PLAYER_DATA":
                this.mgr.ws.send(JSON.stringify({
                    type: "PLAYER_DATA",
                    other_id: this.mgr.other_id,
                    x: this.node.x,
                    y: this.node.y,
                    animateState: this.animateState.name
                }));
                break;
            case "PLACE_BUBBLE":
                console.log(additionalData);
                this.mgr.ws.send(JSON.stringify({
                    type: type,
                    other_id: this.mgr.other_id,
                    x: additionalData[0],
                    y: additionalData[1]
                }));
                break;
            case "PLAYER_IN_BUBBLE":
                this.mgr.ws.send(JSON.stringify({
                    type: type,
                    other_id: this.mgr.other_id,
                    x: additionalData[0],
                    y: additionalData[1]
                }));
                break;
            case "PLAYER_DIE":
                this.mgr.ws.send(JSON.stringify({
                    type: type,
                    other_id: this.mgr.other_id,
                }));
                break;
            case "REBORN":
                this.mgr.ws.send(JSON.stringify({
                    type: type,
                    other_id: this.mgr.other_id,
                }));
                break;
            default:
                break;
        }
    };
    player.prototype.onKeyDown = function (event) {
        if (event.keyCode == cc.macro.KEY.up) {
            this.key_up = true;
            this.key_down = false;
            this.key_left = false;
            this.key_right = false;
            this.face_up = true;
            this.face_down = false;
            this.face_left = false;
            this.face_right = false;
        }
        else if (event.keyCode == cc.macro.KEY.down) {
            this.key_up = false;
            this.key_down = true;
            this.key_left = false;
            this.key_right = false;
            this.face_up = false;
            this.face_down = true;
            this.face_left = false;
            this.face_right = false;
        }
        else if (event.keyCode == cc.macro.KEY.left) {
            this.key_up = false;
            this.key_down = false;
            this.key_left = true;
            this.key_right = false;
            this.face_up = false;
            this.face_down = false;
            this.face_left = true;
            this.face_right = false;
        }
        else if (event.keyCode == cc.macro.KEY.right) {
            this.key_up = false;
            this.key_down = false;
            this.key_left = false;
            this.key_right = true;
            this.face_up = false;
            this.face_down = false;
            this.face_left = false;
            this.face_right = true;
        }
        if (event.keyCode == cc.macro.KEY.space)
            this.key_space = true;
        //用道具(針)
        if (event.keyCode == cc.macro.KEY.num1) {
            if (this.has_reborn)
                this.reborn();
        }
    };
    player.prototype.onKeyUp = function (event) {
        if (event.keyCode == cc.macro.KEY.up)
            this.key_up = false;
        else if (event.keyCode == cc.macro.KEY.down)
            this.key_down = false;
        else if (event.keyCode == cc.macro.KEY.left)
            this.key_left = false;
        else if (event.keyCode == cc.macro.KEY.right)
            this.key_right = false;
    };
    player.prototype.player_move = function (dt) {
        if (this.stuck)
            return;
        this.speedX = 0;
        this.speedY = 0;
        if (this.key_up)
            this.speedY = 100 + this.speedY_add;
        else if (this.key_down)
            this.speedY = -(100 + this.speedY_add);
        else if (this.key_left)
            this.speedX = -(100 + this.speedX_add);
        else if (this.key_right)
            this.speedX = 100 + this.speedX_add;
        this.node.x += this.speedX * dt;
        this.node.y += this.speedY * dt;
    };
    player.prototype.player_face = function (dt) {
        if (this.stuck)
            return;
        //角色靜止動畫
        if (cc.find("GameMgr").getComponent('GameMgr').buyflag == 0) {
            if (this.speedX == 0 && this.speedY == 0) {
                if (this.face_up)
                    this.animateState = this.anim.play('pirate_idle_up');
                else if (this.face_down)
                    this.animateState = this.anim.play('pirate_idle_down');
                else if (this.face_left)
                    this.animateState = this.anim.play('pirate_idle_left');
                else if (this.face_right)
                    this.animateState = this.anim.play('pirate_idle_right');
            }
            else {
                if (this.face_up) {
                    if (this.animateState.name != 'pirate_move_up') {
                        this.animateState = this.anim.play('pirate_move_up');
                    }
                }
                else if (this.face_down) {
                    if (this.animateState.name != 'pirate_move_down') {
                        this.animateState = this.anim.play('pirate_move_down');
                    }
                }
                else if (this.face_left) {
                    if (this.animateState.name != 'pirate_move_left') {
                        this.animateState = this.anim.play('pirate_move_left');
                    }
                }
                else if (this.face_right) {
                    if (this.animateState.name != 'pirate_move_right') {
                        this.animateState = this.anim.play('pirate_move_right');
                    }
                }
            }
        }
        else {
            if (this.speedX == 0 && this.speedY == 0) {
                if (this.face_up)
                    this.animateState = this.anim.play('player_idle_up');
                else if (this.face_down)
                    this.animateState = this.anim.play('player_idle_down');
                else if (this.face_left)
                    this.animateState = this.anim.play('player_idle_left');
                else if (this.face_right)
                    this.animateState = this.anim.play('player_idle_right');
            }
            else {
                if (this.face_up) {
                    if (this.animateState.name != 'player_move_up') {
                        this.animateState = this.anim.play('player_move_up');
                    }
                }
                else if (this.face_down) {
                    if (this.animateState.name != 'player_move_down') {
                        this.animateState = this.anim.play('player_move_down');
                    }
                }
                else if (this.face_left) {
                    if (this.animateState.name != 'player_move_left') {
                        this.animateState = this.anim.play('player_move_left');
                    }
                }
                else if (this.face_right) {
                    if (this.animateState.name != 'player_move_right') {
                        this.animateState = this.anim.play('player_move_right');
                    }
                }
            }
        }
    };
    player.prototype.place_bubble = function (dt) {
        if (this.stuck) {
            this.key_space = false;
            return;
        }
        var dif_x = 999, dif_y = 999;
        var near_x, near_y;
        if (this.key_space) {
            this.key_space = false;
            //目前自己放在場上的水球
            var exist_bubble = cc.find('Canvas/bubble').children;
            //場上不能同時超過持有的水球數量
            if (exist_bubble.length == this.bubble_num)
                return;
            for (var i = 0; i < 15; i++) {
                if (Math.abs(this.node.x - this.map_x[i]) < dif_x) {
                    dif_x = Math.abs(this.node.x - this.map_x[i]);
                    near_x = this.map_x[i];
                }
            }
            for (var i = 0; i < 13; i++) {
                if (Math.abs(this.node.y - this.map_y[i]) < dif_y) {
                    dif_y = Math.abs(this.node.y - this.map_y[i]);
                    near_y = this.map_y[i];
                }
            }
            //有水球的地方不能再放水球
            for (var i = 0; i < exist_bubble.length; i++) {
                if (exist_bubble[i].x == near_x && exist_bubble[i].y == near_y)
                    return;
            }
            //////////////// 
            var bubble = cc.instantiate(this.bubble);
            bubble.x = near_x;
            bubble.y = near_y;
            cc.find('Canvas/bubble').addChild(bubble);
            cc.audioEngine.playEffect(this.sound[2], false);
            // 傳水球位置到server
            this.sendDataToServer("PLACE_BUBBLE", bubble.x, bubble.y);
        }
    };
    player.prototype.onPreSolve = function (contact, self, other) {
        if (other.node.parent.name == 'block') {
            if (this.stuck)
                contact.disabled = true;
            else
                contact.disabled = false;
        }
    };
    player.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'flow_left_end' || other.node.name == 'flow_right_end' || other.node.name == 'flow_up_end' || other.node.name == 'flow_down_end'
            || other.node.name == 'flow_left' || other.node.name == 'flow_right' || other.node.name == 'flow_up' || other.node.name == 'flow_down') {
            if (this.already_in_bubble)
                return;
            this.already_in_bubble = true;
            cc.audioEngine.playEffect(this.sound[3], false);
            this.stuck = true;
            if (cc.find("GameMgr").getComponent('GameMgr').buyflag == 0)
                this.animateState = this.anim.play('pirate_in_bubble');
            else
                this.animateState = this.anim.play('player_in_bubble');
            this.node.opacity = 255;
            //被水球包起來
            var player_in_bubble = cc.instantiate(this.player_in_bubble);
            player_in_bubble.x = this.node.x;
            player_in_bubble.y = this.node.y;
            cc.find('Canvas').addChild(player_in_bubble);
            var action = cc.fadeOut(4);
            this.node.runAction(action);
            this.node.parent = player_in_bubble;
            var bubble_move = cc.repeatForever(cc.sequence(cc.moveBy(0.7, 0, -10), cc.moveBy(0.7, 0, 10)));
            this.node.parent.runAction(bubble_move);
            var player_move = cc.repeatForever(cc.sequence(cc.moveBy(0.7, 0, -10), cc.moveBy(0.7, 0, 10)));
            this.node.runAction(player_move);
            ////
            this.scheduleOnce(this.lose, 5);
            this.scheduleOnce(this.explode, 4.5);
            this.scheduleOnce(this.explode_sound, 4.5);
            this.sendDataToServer("PLAYER_IN_BUBBLE", player_in_bubble.x, player_in_bubble.y);
        }
        if (other.node.name == 'other_prefab') {
            contact.disabled = true;
        }
        //道具
        if (other.node.name == 'add_num') {
            cc.audioEngine.playEffect(this.sound[6], false);
            this.bubble_num++;
        }
        if (other.node.name == 'add_power') {
            cc.audioEngine.playEffect(this.sound[6], false);
            this.bubble_power++;
        }
        if (other.node.name == 'add_speed') {
            cc.audioEngine.playEffect(this.sound[6], false);
            this.speedX_add += 25;
            this.speedY_add += 25;
        }
    };
    player.prototype.lose = function () {
        cc.audioEngine.playMusic(this.sound[7], false);
        cc.find('Canvas/change_scene').getComponent('change_scene').die = true;
        var action = cc.moveBy(3, 0, -300);
        cc.find('Canvas/lose').runAction(action);
        this.node.parent.destroy();
    };
    player.prototype.explode = function () {
        this.node.parent.getComponent(cc.Animation).play();
    };
    player.prototype.reborn = function () {
        if (this.node.parent.name != 'Canvas') {
            if (this.pin == 0)
                return;
            this.pin--;
            this.has_reborn = false;
            this.node.stopAllActions();
            this.node.parent.stopAllActions();
            this.unschedule(this.lose);
            this.unschedule(this.explode);
            this.unschedule(this.explode_sound);
            this.explode();
            this.scheduleOnce(function () {
                this.x_before_reborn = this.node.x;
                this.y_before_reborn = this.node.y;
                this.already_in_bubble = false;
                this.node.opacity = 255;
                this.stuck = false;
                var ini_parent = this.node.parent;
                this.node.parent = cc.find('Canvas');
                this.node.x = this.x_before_reborn;
                this.node.y = this.y_before_reborn;
                ini_parent.destroy();
                this.has_reborn = true;
            }, 0.3);
            var email = cc.find("GameMgr").getComponent('GameMgr').email;
            firebase.database().ref(email + '/pin').set(this.pin);
            this.sendDataToServer('REBORN');
        }
    };
    player.prototype.explode_sound = function () {
        cc.audioEngine.playEffect(this.sound[4], false);
    };
    __decorate([
        property(cc.Prefab)
    ], player.prototype, "bubble", void 0);
    __decorate([
        property(cc.Prefab)
    ], player.prototype, "player_in_bubble", void 0);
    player = __decorate([
        ccclass
    ], player);
    return player;
}(cc.Component));
exports.default = player;

cc._RF.pop();