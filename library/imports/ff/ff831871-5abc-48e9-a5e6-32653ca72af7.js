"use strict";
cc._RF.push(module, 'ff831hxWrxI6aXmMmU8pyr3', 'item_gen');
// script/item_gen.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameMgr_1 = require("./GameMgr");
var item_gen = /** @class */ (function (_super) {
    __extends(item_gen, _super);
    function item_gen() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.block = null;
        _this.item_gen = [];
        _this.which_item = [];
        _this.mgr = null;
        _this.final_item_x = [];
        _this.final_item_y = [];
        _this.final_which_item = [];
        _this.already_push = false;
        return _this;
    }
    item_gen.prototype.onLoad = function () {
        this.mgr = cc.find("GameMgr").getComponent(GameMgr_1.default);
        this.block = cc.find('Canvas/block').children;
        for (var i = 0; i < this.block.length; i++) {
            if (this.block[i].name == 'redblock' || this.block[i].name == 'orangeblock' || this.block[i].name == 'move_block') {
                var item_gen = Math.floor(Math.random() * 2);
                var which_item = Math.floor(Math.random() * 3);
                this.item_gen.push(item_gen);
                this.which_item.push(which_item);
            }
            else {
                this.item_gen.push(-1);
                this.which_item.push(-1);
            }
        }
    };
    item_gen.prototype.update = function () {
        this.sendDataToServer('ITEM', this.item_gen, this.which_item);
        if (this.already_push)
            return;
        if (cc.find('GameMgr').getComponent(GameMgr_1.default).have_item) {
            for (var i = 0; i < this.block.length; i++) {
                if (this.block[i].name == 'redblock' || this.block[i].name == 'orangeblock' || this.block[i].name == 'move_block') {
                    if (this.item_gen[i] == 0 && cc.find('GameMgr').getComponent(GameMgr_1.default).item_gen[i] == 0) {
                        this.block[i].getComponent('block').have_item = 0;
                        if (this.which_item[i] <= cc.find('GameMgr').getComponent(GameMgr_1.default).which_item[i])
                            this.block[i].getComponent('block').which_item = this.which_item[i];
                        else
                            this.block[i].getComponent('block').which_item = cc.find('GameMgr').getComponent(GameMgr_1.default).which_item[i];
                    }
                }
            }
            this.already_push = true;
        }
    };
    item_gen.prototype.sendDataToServer = function (type) {
        var additionalData = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additionalData[_i - 1] = arguments[_i];
        }
        switch (type) {
            case "ITEM":
                this.mgr.ws.send(JSON.stringify({
                    type: "ITEM",
                    other_id: this.mgr.other_id,
                    item_gen: additionalData[0],
                    which_item: additionalData[1]
                }));
                break;
            default:
                break;
        }
    };
    item_gen = __decorate([
        ccclass
    ], item_gen);
    return item_gen;
}(cc.Component));
exports.default = item_gen;

cc._RF.pop();