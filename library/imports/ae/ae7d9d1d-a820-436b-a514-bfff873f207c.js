"use strict";
cc._RF.push(module, 'ae7d90dqCBDa6UUv/+HPyB8', 'shop');
// script/shop.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameMgr_1 = require("./GameMgr");
var Shop = /** @class */ (function (_super) {
    __extends(Shop, _super);
    function Shop() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.lobby_button = null;
        _this.skin_button = null;
        _this.pin_button = null;
        _this.fivepin_button = null;
        _this.moneyNode = null;
        _this.pinNode = null;
        _this.sellout = null;
        _this.money = null;
        _this.pin = null;
        _this.buyflag = 1;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Shop.prototype.onLoad = function () {
        var _this = this;
        this.lobby_button.on(cc.Node.EventType.MOUSE_DOWN, this.toLobby, this);
        this.skin_button.on(cc.Node.EventType.MOUSE_DOWN, this.buyskin, this);
        this.pin_button.on(cc.Node.EventType.MOUSE_DOWN, this.buypin, this);
        this.fivepin_button.on(cc.Node.EventType.MOUSE_DOWN, this.buy5pin, this);
        var email = cc.find("GameMgr").getComponent(GameMgr_1.default).email;
        firebase.database().ref(email).once('value', function (snapshot) {
            _this.money = snapshot.val().money;
            _this.pin = snapshot.val().pin;
            _this.buyflag = snapshot.val().buyflag;
        });
        cc.find("sellout").active = false;
    };
    Shop.prototype.start = function () {
    };
    Shop.prototype.toLobby = function () {
        cc.director.loadScene('lobby');
    };
    Shop.prototype.buyskin = function () {
        if (this.money >= 9999 && this.buyflag == 1) {
            this.money -= 9999;
            this.buyflag = 0;
            cc.find("sellout").active = true;
        }
        var email = cc.find("GameMgr").getComponent(GameMgr_1.default).email;
        firebase.database().ref(email + '/money').set(this.money);
        firebase.database().ref(email + '/buyflag').set(this.buyflag);
    };
    Shop.prototype.buy5pin = function () {
        if (this.money >= 450) {
            this.money -= 450;
            this.pin += 5;
        }
        var email = cc.find("GameMgr").getComponent(GameMgr_1.default).email;
        firebase.database().ref(email + '/money').set(this.money);
        firebase.database().ref(email + '/pin').set(this.pin);
    };
    Shop.prototype.buypin = function () {
        if (this.money >= 100) {
            this.money -= 100;
            this.pin += 1;
        }
        var email = cc.find("GameMgr").getComponent(GameMgr_1.default).email;
        firebase.database().ref(email + '/money').set(this.money);
        firebase.database().ref(email + '/pin').set(this.pin);
    };
    Shop.prototype.update = function (dt) {
        var money_string = " " + this.money;
        var pin_string = " " + this.pin;
        cc.find("moneynode").getComponent(cc.Label).string = money_string;
        cc.find("pinnode").getComponent(cc.Label).string = pin_string;
        if (!this.buyflag)
            cc.find("sellout").active = true;
    };
    __decorate([
        property(cc.Node)
    ], Shop.prototype, "lobby_button", void 0);
    __decorate([
        property(cc.Node)
    ], Shop.prototype, "skin_button", void 0);
    __decorate([
        property(cc.Node)
    ], Shop.prototype, "pin_button", void 0);
    __decorate([
        property(cc.Node)
    ], Shop.prototype, "fivepin_button", void 0);
    __decorate([
        property(cc.Node)
    ], Shop.prototype, "moneyNode", void 0);
    __decorate([
        property(cc.Node)
    ], Shop.prototype, "pinNode", void 0);
    __decorate([
        property(cc.Node)
    ], Shop.prototype, "sellout", void 0);
    Shop = __decorate([
        ccclass
    ], Shop);
    return Shop;
}(cc.Component));
exports.default = Shop;

cc._RF.pop();