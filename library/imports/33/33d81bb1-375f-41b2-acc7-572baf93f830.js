"use strict";
cc._RF.push(module, '33d81uxN19BsqzHVyuvk/gw', 'player_in_bubble');
// script/player_in_bubble.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var player_in_bubble = /** @class */ (function (_super) {
    __extends(player_in_bubble, _super);
    function player_in_bubble() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.other_player_contact = false;
        return _this;
    }
    player_in_bubble.prototype.onLoad = function () {
    };
    player_in_bubble.prototype.start = function () {
    };
    // update (dt) {}
    player_in_bubble.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'other_prefab') {
            this.other_player_contact = true;
        }
    };
    player_in_bubble = __decorate([
        ccclass
    ], player_in_bubble);
    return player_in_bubble;
}(cc.Component));
exports.default = player_in_bubble;

cc._RF.pop();