"use strict";
cc._RF.push(module, 'de51fuMUopAI5Vdseq8BhQ6', 'grass');
// script/grass.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var grass = /** @class */ (function (_super) {
    __extends(grass, _super);
    function grass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    grass.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'player' || other.node.name == 'other_prefab') {
            other.node.opacity = 0;
        }
        if (other.node.name == 'flow_left_end' || other.node.name == 'flow_right_end' || other.node.name == 'flow_up_end' || other.node.name == 'flow_down_end'
            || other.node.name == 'flow_left' || other.node.name == 'flow_right' || other.node.name == 'flow_up' || other.node.name == 'flow_down' || other.node.name == 'flow_center') {
            this.node.destroy();
        }
    };
    grass.prototype.onEndContact = function (contact, self, other) {
        if (other.node.name == 'player' || other.node.name == 'other_prefab') {
            other.node.opacity = 255;
        }
    };
    grass = __decorate([
        ccclass
    ], grass);
    return grass;
}(cc.Component));
exports.default = grass;

cc._RF.pop();