"use strict";
cc._RF.push(module, '3615anu3sNCA4GO0ruuOfu4', 'block_in_boss');
// script/boss1/block_in_boss.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var block_in_boss = /** @class */ (function (_super) {
    __extends(block_in_boss, _super);
    function block_in_boss() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.add_num = null;
        _this.add_power = null;
        _this.add_speed = null;
        _this.anim = null;
        return _this;
    }
    block_in_boss.prototype.onLoad = function () {
        this.anim = this.node.getComponent(cc.Animation);
    };
    block_in_boss.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == 'flow_left_end' || other.node.name == 'flow_right_end' || other.node.name == 'flow_up_end' || other.node.name == 'flow_down_end'
            || other.node.name == 'flow_left' || other.node.name == 'flow_right' || other.node.name == 'flow_up' || other.node.name == 'flow_down') {
            this.scheduleOnce(this.block_destroy, 0.65);
            this.anim.play();
        }
    };
    block_in_boss.prototype.block_destroy = function () {
        var item_gen = Math.floor(Math.random() * 2);
        if (item_gen == 0) {
            var which_item = Math.floor(Math.random() * 3);
            var item;
            if (which_item == 0) {
                item = cc.instantiate(this.add_num);
            }
            else if (which_item == 1) {
                item = cc.instantiate(this.add_power);
            }
            else if (which_item == 2) {
                item = cc.instantiate(this.add_speed);
            }
            item.x = this.node.x;
            item.y = this.node.y;
            item.parent = cc.find('Canvas/item');
        }
        this.node.destroy();
    };
    __decorate([
        property(cc.Prefab)
    ], block_in_boss.prototype, "add_num", void 0);
    __decorate([
        property(cc.Prefab)
    ], block_in_boss.prototype, "add_power", void 0);
    __decorate([
        property(cc.Prefab)
    ], block_in_boss.prototype, "add_speed", void 0);
    block_in_boss = __decorate([
        ccclass
    ], block_in_boss);
    return block_in_boss;
}(cc.Component));
exports.default = block_in_boss;

cc._RF.pop();