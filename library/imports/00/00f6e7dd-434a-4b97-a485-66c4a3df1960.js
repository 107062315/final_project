"use strict";
cc._RF.push(module, '00f6efdQ0pLl6SFZsSj3xlg', 'flow');
// script/flow.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var flow = /** @class */ (function (_super) {
    __extends(flow, _super);
    function flow() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // LIFE-CYCLE CALLBACKS:
    flow.prototype.start = function () {
        this.scheduleOnce(function () {
            this.node.destroy();
        }, 0.5);
    };
    flow = __decorate([
        ccclass
    ], flow);
    return flow;
}(cc.Component));
exports.default = flow;

cc._RF.pop();